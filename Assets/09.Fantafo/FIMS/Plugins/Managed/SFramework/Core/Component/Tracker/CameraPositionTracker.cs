﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class CameraPositionTracker : AbsTracker
{
    [Space]
    public Camera _target;
    public Vector3 localPosition;
    public Vector3 rotationOffset;
    Quaternion _rotationOffset;

    protected override void Start()
    {
        base.Start();
        _rotationOffset = Quaternion.Euler(rotationOffset);
    }
    private void OnValidate()
    {
        Start();
    }

    public override void OnReposition()
    {
        Transform target = (_target != null ? _target : Camera.main).transform;
        transform.position = target.position + (target.rotation * localPosition);
        transform.rotation = target.rotation * _rotationOffset;
    }
}
