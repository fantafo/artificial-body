﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class LookAtTransform : AbsTracker
{
    public Transform _target;
    public Vector3 _offset;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null)
            return;
#endif
        transform.rotation = Quaternion.LookRotation(_target.position - transform.position) * Quaternion.Euler(_offset);
        //transform.LookAt(_target);
    }
}
