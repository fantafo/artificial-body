﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

public static class RenderTextureExtensions
{
    public static Texture2D ToTexture(this RenderTexture rt)
    {
        TextureFormat format;
        switch (rt.format)
        {
            case RenderTextureFormat.Depth:
                format = TextureFormat.Alpha8;
                break;
            case RenderTextureFormat.ARGBHalf:
                format = TextureFormat.ARGB4444;
                break;
            case RenderTextureFormat.Shadowmap:
                format = TextureFormat.Alpha8;
                break;
            case RenderTextureFormat.RGB565:
                format = TextureFormat.RGB565;
                break;
            case RenderTextureFormat.ARGB4444:
                format = TextureFormat.ARGB4444;
                break;
            case RenderTextureFormat.ARGBFloat:
                format = TextureFormat.RGBAFloat;
                break;
            case RenderTextureFormat.RGFloat:
                format = TextureFormat.RGFloat;
                break;
            case RenderTextureFormat.RGHalf:
                format = TextureFormat.RGHalf;
                break;
            case RenderTextureFormat.RFloat:
                format = TextureFormat.RFloat;
                break;
            case RenderTextureFormat.RHalf:
                format = TextureFormat.RHalf;
                break;
            case RenderTextureFormat.BGRA32:
                format = TextureFormat.BGRA32;
                break;
            default:
                format = TextureFormat.ARGB32;
                break;
        }
        Texture2D texture = new Texture2D(rt.width, rt.height, format, false, true);
        RenderTexture.active = rt;
        texture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        RenderTexture.active = null;

        return texture;
    }
    public static byte[] EncodePNG(this RenderTexture rt)
    {
        return rt.ToTexture().EncodeToPNG();
    }
}