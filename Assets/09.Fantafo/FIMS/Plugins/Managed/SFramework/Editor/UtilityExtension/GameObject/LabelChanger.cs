﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions.Editors
{
    public class LabelChanger
    {
        [MenuItem("GameObject/Icon/None")]
        public static void LabelNone()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, null);
            }
        }
        [MenuItem("GameObject/Icon/Gray/Label")]
        public static void LabelGray()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Gray);
            }
        }
        [MenuItem("GameObject/Icon/Blue/Label")]
        public static void LabelBlue()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Blue);
            }
        }
        [MenuItem("GameObject/Icon/Teal/Label")]
        public static void LabelTeal()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Teal);
            }
        }
        [MenuItem("GameObject/Icon/Green/Label")]
        public static void LabelGreen()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Green);
            }
        }
        [MenuItem("GameObject/Icon/Yellow/Label")]
        public static void LabelYellow()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Yellow);
            }
        }
        [MenuItem("GameObject/Icon/Orange/Label")]
        public static void LabelOrange()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Orange);
            }
        }
        [MenuItem("GameObject/Icon/Red/Label")]
        public static void LabelRed()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Red);
            }
        }
        [MenuItem("GameObject/Icon/Purple/Label")]
        public static void LabelPurple()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, LabelIcon.Purple);
            }
        }
        [MenuItem("GameObject/Icon/Gray/Circle")]
        public static void LabelCircleGray()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleGray);
            }
        }
        [MenuItem("GameObject/Icon/Blue/Circle")]
        public static void LabelCircleBlue()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleBlue);
            }
        }
        [MenuItem("GameObject/Icon/Teal/Circle")]
        public static void LabelCircleTeal()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleTeal);
            }
        }
        [MenuItem("GameObject/Icon/Green/Circle")]
        public static void LabelCircleGreen()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleGreen);
            }
        }
        [MenuItem("GameObject/Icon/Yellow/Circle")]
        public static void LabelCircleYellow()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleYellow);
            }
        }
        [MenuItem("GameObject/Icon/Orange/Circle")]
        public static void LabelCircleOrange()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleOrange);
            }
        }
        [MenuItem("GameObject/Icon/Red/Circle")]
        public static void LabelCircleRed()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CircleRed);
            }
        }
        [MenuItem("GameObject/Icon/Purple/Circle")]
        public static void LabelCirclePurple()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.CirclePurple);
            }
        }
        [MenuItem("GameObject/Icon/Gray/Diamond")]
        public static void LabelDiamondGray()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondGray);
            }
        }
        [MenuItem("GameObject/Icon/Blue/Diamond")]
        public static void LabelDiamondBlue()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondBlue);
            }
        }
        [MenuItem("GameObject/Icon/Teal/Diamond")]
        public static void LabelDiamondTeal()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondTeal);
            }
        }
        [MenuItem("GameObject/Icon/Green/Diamond")]
        public static void LabelDiamondGreen()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondGreen);
            }
        }
        [MenuItem("GameObject/Icon/Yellow/Diamond")]
        public static void LabelDiamondYellow()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondYellow);
            }
        }
        [MenuItem("GameObject/Icon/Orange/Diamond")]
        public static void LabelDiamondOrange()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondOrange);
            }
        }
        [MenuItem("GameObject/Icon/Red/Diamond")]
        public static void LabelDiamondRed()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondRed);
            }
        }
        [MenuItem("GameObject/Icon/Purple/Diamond")]
        public static void LabelDiamondPurple()
        {
            foreach (var g in Selection.gameObjects)
            {
                SetIcon(g, Icon.DiamondPurple);
            }
        }


        public enum LabelIcon
        {
            Gray = 0,
            Blue,
            Teal,
            Green,
            Yellow,
            Orange,
            Red,
            Purple
        }

        public enum Icon
        {
            CircleGray = 0,
            CircleBlue,
            CircleTeal,
            CircleGreen,
            CircleYellow,
            CircleOrange,
            CircleRed,
            CirclePurple,
            DiamondGray,
            DiamondBlue,
            DiamondTeal,
            DiamondGreen,
            DiamondYellow,
            DiamondOrange,
            DiamondRed,
            DiamondPurple
        }

        private static GUIContent[] labelIcons;
        private static GUIContent[] largeIcons;

        public static void SetIcon(GameObject gObj, LabelIcon icon)
        {
            if (labelIcons == null)
            {
                labelIcons = GetTextures("sv_label_", string.Empty, 0, 8);
            }

            SetIcon(gObj, labelIcons[(int)icon].image as Texture2D);
        }

        public static void SetIcon(GameObject gObj, Icon icon)
        {
            if (largeIcons == null)
            {
                largeIcons = GetTextures("sv_icon_dot", "_pix16_gizmo", 0, 16);
            }

            SetIcon(gObj, largeIcons[(int)icon].image as Texture2D);
        }

        private static void SetIcon(GameObject gObj, Texture2D texture)
        {
            var ty = typeof(EditorGUIUtility);
            var mi = ty.GetMethod("SetIconForObject", BindingFlags.NonPublic | BindingFlags.Static);
            mi.Invoke(null, new object[] { gObj, texture });
        }

        private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
        {
            GUIContent[] guiContentArray = new GUIContent[count];

            for (int index = 0; index < count; ++index)
            {
                guiContentArray[index] = EditorGUIUtility.IconContent(baseName + (object)(startIndex + index) + postFix);
            }

            return guiContentArray;
        }

    }
}