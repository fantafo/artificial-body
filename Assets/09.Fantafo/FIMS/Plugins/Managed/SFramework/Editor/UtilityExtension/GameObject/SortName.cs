﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class SortName : BaseExtensionEditor
    {
        [MenuItem("GameObject/Hierarchy/Sort Name")]
        public static void TransformReverseToHierarchy()
        {
            if (!init("Reverse", 2))
                return;
            if (transforms == null || transforms.Length == 0)
                return;


            Transform parent = transforms[0].parent;
            if (!parent)
            {
                Debug.LogWarning("Root 오브젝트는 정렬할 수 없습니다.");
                return;
            }

            foreach (var t in transforms)
                if (t.parent != parent)
                {
                    Debug.LogWarning("같은 부모의 오브젝트만 정렬할 수 있습니다.");
                    return;
                }

            Array.Sort(transforms, (a, b) => a.name.CompareTo(b.name));

            foreach (Transform trans in transforms)
            {
                trans.SetAsLastSibling();
            }
        }

        [MenuItem("GameObject/Hierarchy/Sort Name (int)")]
        public static void TransformReverseToHierarchyInteger()
        {
            if (!init("Reverse", 2))
                return;
            if (transforms == null || transforms.Length == 0)
                return;


            Transform parent = transforms[0].parent;
            if (!parent)
            {
                Debug.LogWarning("Root 오브젝트는 정렬할 수 없습니다.");
                return;
            }

            foreach (var t in transforms)
                if (t.parent != parent)
                {
                    Debug.LogWarning("같은 부모의 오브젝트만 정렬할 수 있습니다.");
                    return;
                }

            Array.Sort(transforms, (a, b) => int.Parse(a.name).CompareTo(int.Parse(b.name)));

            foreach (Transform trans in transforms)
            {
                trans.SetAsLastSibling();
            }
        }


    }
}