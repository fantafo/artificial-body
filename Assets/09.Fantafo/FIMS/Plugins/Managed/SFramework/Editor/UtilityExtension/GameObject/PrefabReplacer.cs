﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Sions.Editors;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class PrefabReplacer : SearchableEditorWindow
    {


        [MenuItem("GameObject/Prefab/Reconnect")]
        public static void Showing()
        {
            EditorWindow.GetWindowWithRect<PrefabReplacer>(new Rect(0, 0, 300, 100), true);
        }

        public GameObject select;

        public void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            GUILayout.Space(10);
            select = (GameObject)EditorGUILayout.ObjectField("Prefab", select, typeof(GameObject), true);
            if (select)
            {
                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Connect"))
                {
                    foreach (var go in Selection.gameObjects)
                    {
                        PrefabUtility.ConnectGameObjectToPrefab(go, select);
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

    }
}