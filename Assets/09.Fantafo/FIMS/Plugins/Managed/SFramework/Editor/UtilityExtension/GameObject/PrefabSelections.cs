﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class PrefabSelections : BaseExtensionEditor
    {
        [MenuItem("GameObject/Prefab/Multi Apply")]
        public static void PrefabApply()
        {
            if (!init("MultiApply", 0))
                return;
            if (transforms == null || transforms.Length == 0)
                return;

            foreach (var t in transforms)
            {
                var root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(t.gameObject);
                var prefab = PrefabUtility.GetPrefabParent(root);

                PrefabUtility.ReplacePrefab(root, prefab, ReplacePrefabOptions.ConnectToPrefab);
            }
        }

        [MenuItem("GameObject/Prefab/Multi Revert")]
        public static void PrefabRevert()
        {
            if (!init("MultiRevert", 0))
                return;
            if (transforms == null || transforms.Length == 0)
                return;

            foreach (var t in transforms)
            {
                var root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(t.gameObject);
                PrefabUtility.GetPrefabParent(root);

                PrefabUtility.RevertPrefabInstance(t.gameObject);
            }
        }




        //[MenuItem("GameObject/Prefab/Set Prefab")]
        //public static void SetPrefab()
        //{
        //    if (!init("MultiRevert", 0)) return;
        //    if (transforms == null || transforms.Length == 0)
        //        return;

        //    Transform parent = transforms[0].parent;
        //    if (!parent)
        //    {
        //        Debug.LogWarning("Root 오브젝트는 반전할 수 없습니다.");
        //        return;
        //    }

        //    foreach (var t in transforms)
        //    {
        //        var root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(t.gameObject);
        //        var prefab = PrefabUtility.GetPrefabParent(root);

        //        PrefabUtility.RevertPrefabInstance(t.gameObject);
        //    }
        //}




    }
}