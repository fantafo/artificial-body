﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Sions;

/// <summary>
/// Author : 정상철
/// </summary>
public class WorldTransformWindow : EditorWindow
{
    [MenuItem("SF/World Transform %&t", priority = 0x1001)]
    public static void ShowWindow()
    {
        if (focusedWindow is WorldTransformWindow)
        {
            focusedWindow.Close();
        }
        else
        {
            GetWindowWithRect<WorldTransformWindow>(new Rect(0, 0, 440, 110), true);
        }
    }

    double lastUpdate;
    void Update()
    {
        if (lastUpdate + 0.5 < EditorApplication.timeSinceStartup)
        {
            Repaint();
        }
    }

    void OnGUI()
    {
        lastUpdate = EditorApplication.timeSinceStartup;

        var objs = Selection.gameObjects;
        if (objs.Length == 0 || objs.Length > 1)
            return;

        GameObject select = Selection.activeGameObject;
        if (select)
        {
            Transform trans = select.transform;

            EditorGUIUtility.labelWidth = 15;


            Vector3 position = Vector3Field("Position", trans.position);
            if (GUI.changed)
            { GUI.changed = false; trans.position = position; }

            Vector3 euler = Vector3Field("Rotation", trans.eulerAngles);
            if (GUI.changed)
            { GUI.changed = false; trans.eulerAngles = euler; }

            Vector3 scale = Vector3Field("Scale", trans.lossyScale);
            if (GUI.changed)
            {
                GUI.changed = false;
                Vector3 upscale = trans.localScale.Divide(trans.lossyScale);
                trans.localScale = scale.Multiply(upscale);
            }

            GUILayout.Space(5);
            Quaternion rotation = Quaternion3Field("Local Q Rotation", trans.localRotation);
            if (GUI.changed)
            {
                trans.localRotation = rotation;
            }
            rotation = Quaternion3Field("Q Rotation", trans.rotation);
            if (GUI.changed)
            {
                trans.rotation = rotation;
            }


        }
    }

    Vector3 Vector3Field(string name, Vector3 vect)
    {
        bool changed = false;
        GUILayout.BeginHorizontal();
        GUILayout.Label(name, GUILayout.MaxWidth(70), GUILayout.MinWidth(50));
        float v = EditorGUILayout.FloatField("X", vect.x);
        if (GUI.changed)
        { vect.x = v; GUI.changed = false; changed = true; }
        v = EditorGUILayout.FloatField("Y", vect.y);
        if (GUI.changed)
        { vect.y = v; GUI.changed = false; changed = true; }
        v = EditorGUILayout.FloatField("Z", vect.z);
        if (GUI.changed)
        { vect.z = v; GUI.changed = false; changed = true; }

        if (GUILayout.Button("C", GUILayout.Width(20)))
        {
            EditorPrefs.SetString("WorldTransformWindows." + name, string.Format("{0},{1},{2}", vect.x, vect.y, vect.z));
        }
        if (GUILayout.Button("P", GUILayout.Width(20)))
        {
            var txt = EditorPrefs.GetString("WorldTransformWindows." + name).Split(',');
            if (txt.Length == 3)
            {
                vect.x = float.Parse(txt[0]);
                vect.y = float.Parse(txt[1]);
                vect.z = float.Parse(txt[2]);
            }
        }

        GUILayout.EndHorizontal();
        if (changed)
            GUI.changed = true;
        return vect;
    }

    Quaternion Quaternion3Field(string name, Quaternion vect)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(name, GUILayout.MaxWidth(70), GUILayout.MinWidth(50));
        vect.x = EditorGUILayout.FloatField("X", vect.x);
        vect.y = EditorGUILayout.FloatField("Y", vect.y);
        vect.z = EditorGUILayout.FloatField("Z", vect.z);
        vect.w = EditorGUILayout.FloatField("W", vect.w);

        if (GUILayout.Button("C", GUILayout.Width(20)))
        {
            EditorPrefs.SetString("WorldTransformWindows." + name, string.Format("{0},{1},{2},{3}", vect.x, vect.y, vect.z, vect.w));
        }
        if (GUILayout.Button("P", GUILayout.Width(20)))
        {
            var txt = EditorPrefs.GetString("WorldTransformWindows." + name).Split(',');
            if (txt.Length == 4)
            {
                vect.x = float.Parse(txt[0]);
                vect.y = float.Parse(txt[1]);
                vect.z = float.Parse(txt[2]);
                vect.w = float.Parse(txt[3]);
            }
        }
        GUILayout.EndHorizontal();
        return vect;
    }
}