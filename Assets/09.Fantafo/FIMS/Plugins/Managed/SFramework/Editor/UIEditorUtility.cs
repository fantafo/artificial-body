﻿
using UnityEngine;
using UnityEditor;
using System.Collections;
using Sions;
using System.Collections.Generic;

public static class UIEditorUtility
{
    public static bool minimalisticLook;

    public static void SetLabelWidth(float width)
    {
        EditorGUIUtility.labelWidth = width;
    }

    /// <summary>
    /// Draw a distinctly different looking header label
    /// </summary>
    public static bool DrawHeader(string text) { return DrawHeader(text, text, false, UIEditorUtility.minimalisticLook); }

    /// <summary>
    /// Draw a distinctly different looking header label
    /// </summary>
    public static bool DrawHeader(string text, string key) { return DrawHeader(text, key, false, UIEditorUtility.minimalisticLook); }

    /// <summary>
    /// Draw a distinctly different looking header label
    /// </summary>
    public static bool DrawHeader(string text, bool detailed) { return DrawHeader(text, text, false, false, detailed); }

    /// <summary>
    /// Draw a distinctly different looking header label
    /// </summary>
    public static bool DrawHeader(string text, SerializedObject ser, bool def = true) { return DrawHeader(text, text + "-" + ser.targetObject.GetInstanceID(), false, UIEditorUtility.minimalisticLook, def); }

    /// <summary>
    /// Draw a distinctly different looking header label
    /// </summary>
    public static bool DrawHeader(string text, string key, bool forceOn, bool minimalistic, bool def = true)
    {
        bool state = EditorPrefs.GetBool(key, def);

        if (!minimalistic)
            GUILayout.Space(3f);
        if (!forceOn && !state)
            GUI.backgroundColor = new Color(0.8f, 0.8f, 0.8f);
        EditorGUILayout.BeginHorizontal();
        GUI.changed = false;

        if (minimalistic)
        {
            if (state)
                text = "\u25BC" + (char)0x200a + text;
            else
                text = "\u25BA" + (char)0x200a + text;

            EditorGUILayout.BeginHorizontal();
            GUI.contentColor = EditorGUIUtility.isProSkin ? new Color(1f, 1f, 1f, 0.7f) : new Color(0f, 0f, 0f, 0.7f);
            if (!GUILayout.Toggle(true, text, "PreToolbar2", GUILayout.MinWidth(20f)))
                state = !state;
            GUI.contentColor = Color.white;
            EditorGUILayout.EndHorizontal();
        }
        else
        {
            text = "<b><size=11>" + text + "</size></b>";
            if (state)
                text = "\u25BC " + text;
            else
                text = "\u25BA " + text;

            if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
                state = !state;
        }

        if (GUI.changed)
            EditorPrefs.SetBool(key, state);

        if (!minimalistic)
            GUILayout.Space(2f);
        EditorGUILayout.EndHorizontal();
        GUI.backgroundColor = Color.white;
        if (!forceOn && !state)
            GUILayout.Space(3f);
        return state;
    }

    /// <summary>
    /// Begin drawing the content area.
    /// </summary>
    static bool mEndHorizontal = false;

    public static void BeginContents()
    {
        BeginContents(UIEditorUtility.minimalisticLook);
    }

    /// <summary>
    /// Begin drawing the content area.
    /// </summary>
    public static void BeginContents(bool minimalistic)
    {
        if (!minimalistic)
        {
            mEndHorizontal = true;
            GUILayout.BeginHorizontal();
            EditorGUILayout.BeginHorizontal("AS TextArea", GUILayout.MinHeight(10f));
            GUILayout.Space(5f);
        }
        else
        {
            mEndHorizontal = false;
            EditorGUILayout.BeginHorizontal(GUILayout.MinHeight(10f));
            GUILayout.Space(20f);
        }
        GUILayout.BeginVertical();
        GUILayout.Space(5f);
    }

    /// <summary>
    /// End drawing the content area.
    /// </summary>
    public static void EndContents()
    {
        GUILayout.Space(10f);
        GUILayout.EndVertical();
        GUILayout.Space(10);
        EditorGUILayout.EndHorizontal();

        if (mEndHorizontal)
        {
            GUILayout.Space(3f);
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(3f);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>
    public static SerializedProperty DrawProperty(SerializedObject serializedObject, string property, params GUILayoutOption[] options)
    {
        return DrawProperty(null, serializedObject, property, false, options);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>
    public static SerializedProperty DrawProperty(string label, SerializedObject serializedObject, string property, params GUILayoutOption[] options)
    {
        return DrawProperty(label, serializedObject, property, false, options);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>
    public static SerializedProperty DrawPaddedProperty(SerializedObject serializedObject, string property, params GUILayoutOption[] options)
    {
        return DrawProperty(null, serializedObject, property, true, options);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>
    public static SerializedProperty DrawPaddedProperty(string label, SerializedObject serializedObject, string property, params GUILayoutOption[] options)
    {
        return DrawProperty(label, serializedObject, property, true, options);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>
    public static SerializedProperty DrawProperty(string label, SerializedObject serializedObject, string property, bool padding, params GUILayoutOption[] options)
    {
        SerializedProperty sp = serializedObject.FindProperty(property);

        if (sp != null)
        {
            if (UIEditorUtility.minimalisticLook)
                padding = false;

            if (padding)
                EditorGUILayout.BeginHorizontal();

            if (sp.isArray && sp.type != "string")
                DrawArray(serializedObject, property, label ?? property);
            else if (label != null)
                EditorGUILayout.PropertyField(sp, new GUIContent(label), options);
            else
                EditorGUILayout.PropertyField(sp, options);

            if (padding)
            {
                UIEditorUtility.DrawPadding();
                EditorGUILayout.EndHorizontal();
            }
        }
        return sp;
    }

    /// <summary>
    /// Helper function that draws an array property.
    /// </summary>
    public static void DrawArray(SerializedObject obj, string property, string title)
    {
        SerializedProperty sp = obj.FindProperty(property + ".Array.size");

        if (sp != null && UIEditorUtility.DrawHeader(title))
        {
            UIEditorUtility.BeginContents();
            int size = sp.intValue;
            int newSize = EditorGUILayout.IntField("Size", size);
            if (newSize != size)
                obj.FindProperty(property + ".Array.size").intValue = newSize;

            EditorGUI.indentLevel = 1;

            for (int i = 0; i < newSize; i++)
            {
                SerializedProperty p = obj.FindProperty(string.Format("{0}.Array.data[{1}]", property, i));
                EditorGUILayout.PropertyField(p);
            }
            EditorGUI.indentLevel = 0;
            UIEditorUtility.EndContents();
        }
    }
    public static void DrawObjects(SerializedObject obj, string property, string title)
    {
        DrawObjects(obj, property, title, typeof(GameObject));
    }
    public static void DrawObjects(SerializedObject obj, string property, string title, System.Type type)
    {
        SerializedProperty sp = obj.FindProperty(property + ".Array.size");

        if (sp != null && UIEditorUtility.DrawHeader(title))
        {
            UIEditorUtility.BeginContents();
            int size = sp.intValue;

            Object select = null;
            bool isChange = false;
            for (int i = 0; i < size; i++)
            {
                var prop = obj.FindProperty(string.Format("{0}.Array.data[{1}]", property, i));
                var bef = prop.objectReferenceValue;
                select = EditorGUILayout.ObjectField(" Element " + i, bef, type, true);

                if (select == null)
                {
                    var array = obj.FindProperty(property);
                    for (int j = i + 1; j < size; j++)
                    {
                        array.MoveArrayElement(j, j - 1);
                    }
                    obj.FindProperty(property + ".Array.size").intValue = --size;
                }
                else if (bef != select)
                {
                    isChange = true;
                    prop.objectReferenceValue = select;
                }
            }

            //새로 삽입한 오브젝트
            select = EditorGUILayout.ObjectField(" Add", null, type, true);
            if (select || isChange)
            {
                #region Search method
                System.Func<Object, bool> exist = (targetObj) =>
                {
                    for (int j = 0; j < size; j++)
                    {
                        Object searchObj = obj.FindProperty(string.Format("{0}.Array.data[{1}]", property, j)).objectReferenceValue;
                        if (targetObj == searchObj)
                        {
                            return false;
                        }
                    }
                    return true;
                };
                #endregion

                //같이 선택돼 있는 개체들 갑입
                GameObject[] objs = Selection.gameObjects;
                int newCount = -1;

                //선택된 오브젝트에 실제 삽입한 오브젝트가 있는지 검사
                for (int i = 0; i < objs.Length; i++)
                    if (objs[i] == select)
                        newCount = 0;


                // 선택된 오브젝트와 삽입한 오브젝트가 별개라면 삽입한 오브젝트 한개만 처리한다
                if (newCount == -1)
                {
                    if (exist(select))
                    {
                        obj.FindProperty(property + ".Array.size").intValue = size + 1;
                        obj.FindProperty(string.Format("{0}.Array.data[{1}]", property, size++)).objectReferenceValue = select;
                    }
                }
                // 삽입한 오브젝트가 선택된 오브젝트와 같이 있다면 전체 선택 오브젝트를 같이 리스트에 넣는다
                else
                {
                    //중복되는 대상은 null로 할당한다.
                    for (int i = 0; i < objs.Length; i++)
                    {
                        if (exist(objs[i]))
                        {
                            newCount++;
                        }
                        else
                        {
                            objs[i] = null;
                        }
                    }

                    obj.FindProperty(property + ".Array.size").intValue = size + newCount;

                    int addIndex = size;
                    for (int i = 0; i < objs.Length; i++)
                    {
                        if (objs[i] != null)
                            obj.FindProperty(string.Format("{0}.Array.data[{1}]", property, addIndex++)).objectReferenceValue = objs[i];
                    }
                }
            }

            UIEditorUtility.EndContents();
        }
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>

    public static SerializedProperty DrawProperty(string label, SerializedProperty sp, params GUILayoutOption[] options)
    {
        return DrawProperty(label, sp, true, options);
    }

    /// <summary>
    /// Helper function that draws a serialized property.
    /// </summary>

    public static SerializedProperty DrawProperty(string label, SerializedProperty sp, bool padding, params GUILayoutOption[] options)
    {
        if (sp != null)
        {
            if (padding)
                EditorGUILayout.BeginHorizontal();

            if (label != null)
                EditorGUILayout.PropertyField(sp, new GUIContent(label), options);
            else
                EditorGUILayout.PropertyField(sp, options);

            if (padding)
            {
                UIEditorUtility.DrawPadding();
                EditorGUILayout.EndHorizontal();
            }
        }
        return sp;
    }

    /// <summary>
    /// Helper function that draws a compact Vector4.
    /// </summary>

    public static void DrawBorderProperty(string name, SerializedObject serializedObject, string field)
    {
        if (serializedObject.FindProperty(field) != null)
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label(name, GUILayout.Width(75f));

                UIEditorUtility.SetLabelWidth(50f);
                GUILayout.BeginVertical();
                UIEditorUtility.DrawProperty("Left", serializedObject, field + ".x", GUILayout.MinWidth(80f));
                UIEditorUtility.DrawProperty("Bottom", serializedObject, field + ".y", GUILayout.MinWidth(80f));
                GUILayout.EndVertical();

                GUILayout.BeginVertical();
                UIEditorUtility.DrawProperty("Right", serializedObject, field + ".z", GUILayout.MinWidth(80f));
                UIEditorUtility.DrawProperty("Top", serializedObject, field + ".w", GUILayout.MinWidth(80f));
                GUILayout.EndVertical();

                UIEditorUtility.SetLabelWidth(80f);
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Helper function that draws a compact Rect.
    /// </summary>

    public static void DrawRectProperty(string name, SerializedObject serializedObject, string field)
    {
        DrawRectProperty(name, serializedObject, field, 56f, 18f);
    }

    /// <summary>
    /// Helper function that draws a compact Rect.
    /// </summary>

    public static void DrawRectProperty(string name, SerializedObject serializedObject, string field, float labelWidth, float spacing)
    {
        if (serializedObject.FindProperty(field) != null)
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label(name, GUILayout.Width(labelWidth));

                UIEditorUtility.SetLabelWidth(20f);
                GUILayout.BeginVertical();
                UIEditorUtility.DrawProperty("X", serializedObject, field + ".x", GUILayout.MinWidth(50f));
                UIEditorUtility.DrawProperty("Y", serializedObject, field + ".y", GUILayout.MinWidth(50f));
                GUILayout.EndVertical();

                UIEditorUtility.SetLabelWidth(50f);
                GUILayout.BeginVertical();
                UIEditorUtility.DrawProperty("Width", serializedObject, field + ".width", GUILayout.MinWidth(80f));
                UIEditorUtility.DrawProperty("Height", serializedObject, field + ".height", GUILayout.MinWidth(80f));
                GUILayout.EndVertical();

                UIEditorUtility.SetLabelWidth(80f);
                if (spacing != 0f)
                    GUILayout.Space(spacing);
            }
            GUILayout.EndHorizontal();
        }
    }

    /// <summary>
    /// Draw 18 pixel padding on the right-hand side. Used to align fields.
    /// </summary>

    public static void DrawPadding()
    {
        if (!UIEditorUtility.minimalisticLook)
            GUILayout.Space(18f);
    }
    /// <summary>
    /// Create an undo point for the specified objects.
    /// </summary>

    public static void RegisterUndo(string name, params Object[] objects)
    {
        if (objects != null && objects.Length > 0)
        {
            UnityEditor.Undo.RecordObjects(objects, name);

            foreach (Object obj in objects)
            {
                if (obj == null)
                    continue;
                EditorUtility.SetDirty(obj);
            }
        }
    }

    public static void Vector3Field(string label, SerializedObject serializedObject, string property, string focusName)
    {
        var prop = serializedObject.FindProperty(property);
        Vector3 v = prop.vector3Value;

        EditorGUILayout.BeginHorizontal();
        GUI.SetNextControlName(label + System.Guid.NewGuid());
        EditorGUILayout.LabelField(label, GUILayout.Width(EditorGUIUtility.labelWidth));

        float bef = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 15;

        GUI.SetNextControlName(focusName + System.Guid.NewGuid());
        v.x = EditorGUILayout.FloatField("X", v.x);

        GUI.SetNextControlName(focusName + System.Guid.NewGuid());
        v.y = EditorGUILayout.FloatField("Y", v.y);

        GUI.SetNextControlName(focusName + System.Guid.NewGuid());
        v.z = EditorGUILayout.FloatField("Z", v.z);

        prop.vector3Value = v;
        EditorGUIUtility.labelWidth = bef;

        EditorGUILayout.EndHorizontal();
    }

    public static void FloatField(string label, SerializedObject serializedObject, string property, string focusName)
    {
        var prop = serializedObject.FindProperty(property);

        GUI.SetNextControlName(focusName + System.Guid.NewGuid());
        prop.floatValue = EditorGUILayout.FloatField(label, prop.floatValue);

    }

    public static void TextField(string label, SerializedObject serializedObject, string property, string focusName)
    {
        var prop = serializedObject.FindProperty(property);

        GUI.SetNextControlName(focusName + System.Guid.NewGuid());
        prop.stringValue = EditorGUILayout.TextField(label, prop.stringValue);

    }

    public static int BitMaskField(System.Type aType, int val)
    {

        var itemNames = System.Enum.GetNames(aType);
        var itemValues = System.Enum.GetValues(aType) as int[];

        int maskVal = 0;
        for (int i = 0; i < itemValues.Length; i++)
        {
            if (itemValues[i] != 0)
            {
                if ((val & itemValues[i]) == itemValues[i])
                    maskVal |= 1 << i;
            }
            else if (val == 0)
                maskVal |= 1 << i;
        }

        int newMaskVal = EditorGUILayout.MaskField(maskVal, itemNames);
        int changes = maskVal ^ newMaskVal;

        for (int i = 0; i < itemValues.Length; i++)
        {
            if ((changes & (1 << i)) != 0)            // has this list item changed?
            {
                if ((newMaskVal & (1 << i)) != 0)     // has it been set?
                {
                    if (itemValues[i] == 0)           // special case: if "0" is set, just set the val to 0
                    {
                        val = 0;
                        break;
                    }
                    else
                        val |= itemValues[i];
                }
                else                                  // it has been reset
                {
                    val &= ~itemValues[i];
                }
            }
        }
        return val;
    }

    public static int BitMaskField(Rect aPosition, int val, System.Type aType)
    {

        var itemNames = System.Enum.GetNames(aType);
        var itemValues = System.Enum.GetValues(aType) as int[];

        int maskVal = 0;
        for (int i = 0; i < itemValues.Length; i++)
        {
            if (itemValues[i] != 0)
            {
                if ((val & itemValues[i]) == itemValues[i])
                    maskVal |= 1 << i;
            }
            else if (val == 0)
                maskVal |= 1 << i;
        }

        int newMaskVal = EditorGUI.MaskField(aPosition, maskVal, itemNames);
        int changes = maskVal ^ newMaskVal;

        for (int i = 0; i < itemValues.Length; i++)
        {
            if ((changes & (1 << i)) != 0)            // has this list item changed?
            {
                if ((newMaskVal & (1 << i)) != 0)     // has it been set?
                {
                    if (itemValues[i] == 0)           // special case: if "0" is set, just set the val to 0
                    {
                        val = 0;
                        break;
                    }
                    else
                        val |= itemValues[i];
                }
                else                                  // it has been reset
                {
                    val &= ~itemValues[i];
                }
            }
        }
        return val;
    }

    public static void DrawInterfaceLists<T>(SerializedProperty prop)
    {
        if (DrawHeader(prop.displayName, prop.serializedObject, false))
        {

        }
    }

    public static void DrawInterfaces<IObjectType, InterfaceType>(ref IObjectType[] list, string title)
    {
        if (list == null)
            list = new IObjectType[0];

        if (UIEditorUtility.DrawHeader(title))
        {
            //var befChanged = GUI.changed;
            GUI.changed = false;


            UIEditorUtility.BeginContents();
            int size = list.Length;

            List<IObjectType> lists = new List<IObjectType>();

            Object select = null;
            //bool isChange = false;
            for (int i = 0; i < size; i++)
            {
                string name = "  Element " + i;

                select = list[i] as Object;
                if (select)
                {
                    select = EditorGUILayout.ObjectField(name, list[i] as Object, typeof(Object), true);
                    if (select)
                    {
                        if (!object.ReferenceEquals(list[i], select))
                        {
                            if (select is InterfaceType)
                            {
                                if (select is IObjectType)
                                {
                                    IObjectType obj = (IObjectType)(object)select;
                                    if (!lists.Contains(obj))
                                        lists.Add(obj);
                                }
                            }
                            else if (select is GameObject)
                            {
                                foreach (var comp in ((GameObject)select).GetComponents<InterfaceType>())
                                {
                                    if (comp is InterfaceType)
                                    {
                                        if (comp is IObjectType)
                                        {
                                            IObjectType obj = (IObjectType)(object)comp;
                                            if (!lists.Contains(obj))
                                                lists.Add(obj);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!lists.Contains(list[i]))
                                lists.Add(list[i]);
                        }
                    }
                }
                else if (list[i] != null)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.SelectableLabel(name);
                    if (!GUILayout.Button("Unkown Type Remove", "Button"))
                    {
                        if (!lists.Contains(lists[i]))
                            lists.Add(list[i]);
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }

            //새로 삽입한 오브젝트
            select = EditorGUILayout.ObjectField("  Add", null, typeof(Object), true);
            if (select)
            {
                if (select is InterfaceType)
                {
                    if (select is IObjectType)
                    {
                        IObjectType obj = (IObjectType)(object)select;
                        if (!lists.Contains(obj))
                            lists.Add(obj);
                    }
                }
                else if (select is GameObject)
                {
                    foreach (var comp in ((GameObject)select).GetComponents<InterfaceType>())
                    {
                        if (comp is InterfaceType)
                        {
                            if (comp is IObjectType)
                            {
                                IObjectType obj = (IObjectType)(object)comp;
                                if (!lists.Contains(obj))
                                    lists.Add(obj);
                            }
                        }
                    }
                }
            }

            if (GUI.changed)
            {
                list = lists.ToArray();
            }

            UIEditorUtility.EndContents();
        }
    }

}