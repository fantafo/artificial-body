﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
public class SkinnedMeshUnusedBoneSelector
{
    [MenuItem("SF/Tools/Select Skinned Mesh UnusedBone")]
    public static void Select()
    {
        GameObject go = Selection.activeGameObject;
        if (go != null)
        {
            var animator = go.GetComponentInParent<Animator>();
            //var animTrans = animator.transform;
            var renderer = go.GetComponent<SkinnedMeshRenderer>();

            var transforms = animator.GetComponentsInChildren<Transform>();
            List<Transform> bones = new List<Transform>(renderer.bones);
            List<GameObject> notused = new List<GameObject>();

            foreach (var trans in transforms)
            {
                if (trans.GetComponent<SkinnedMeshRenderer>() != null)
                    continue;
                if (trans.GetComponent<Animator>() != null)
                    continue;

                if (!bones.Contains(trans))
                {
                    notused.Add(trans.gameObject);
                }
            }

            for (int i = 0; i < notused.Count; i++)
            {
                Transform t = notused[i].transform.parent;
                if (bones.Contains(t))
                {
                    notused.RemoveAt(i--);
                    break;
                }
            }

            Selection.objects = notused.ToArray();
        }
    }
}