﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                          Value                              //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState valueInt(int from, int to, int duration, Action<int> action)
    {
        return Play<ActionInt, Action<int>, int>(from, to, duration, action);
    }
    public static STweenState value(float from, float to, float duration, Action<float> action)
    {
        return Play<ActionFloat, Action<float>, float>(from, to, duration, action);
    }
    public static STweenState value(Vector2 from, Vector2 to, float duration, Action<Vector2> action)
    {
        return Play<ActionVector2, Action<Vector2>, Vector2>(from, to, duration, action);
    }
    public static STweenState value(Vector3 from, Vector3 to, float duration, Action<Vector3> action)
    {
        return Play<ActionVector3, Action<Vector3>, Vector3>(from, to, duration, action);
    }
    public static STweenState value(Vector4 from, Vector4 to, float duration, Action<Vector4> action)
    {
        return Play<ActionVector4, Action<Vector4>, Vector4>(from, to, duration, action);
    }
    public static STweenState value(Quaternion from, Quaternion to, float duration, Action<Quaternion> action)
    {
        return Play<ActionQuaternion, Action<Quaternion>, Quaternion>(from, to, duration, action);
    }
    public static STweenState value(Color from, Color to, float duration, Action<Color> action)
    {
        return Play<ActionColor, Action<Color>, Color>(from, to, duration, action);
    }
    public static STweenState value(Bounds from, Bounds to, float duration, Action<Bounds> action)
    {
        return Play<ActionBounds, Action<Bounds>, Bounds>(from, to, duration, action);
    }
    public static STweenState value(Rect from, Rect to, float duration, Action<Rect> action)
    {
        return Play<ActionRect, Action<Rect>, Rect>(from, to, duration, action);
    }
    public static STweenState value(RectOffset from, RectOffset to, float duration, Action<RectOffset> action)
    {
        return Play<ActionRectOffset, Action<RectOffset>, RectOffset>(from, to, duration, action);
    }



    public static STweenState value(int to, int duration, Action<int> action)
    {
        return Play<ActionInt, Action<int>, int>(0, to, duration, action);
    }
    public static STweenState value(float to, float duration, Action<float> action)
    {
        return Play<ActionFloat, Action<float>, float>(0, to, duration, action);
    }
    public static STweenState value(Vector2 to, float duration, Action<Vector2> action)
    {
        return Play<ActionVector2, Action<Vector2>, Vector2>(Vector2.zero, to, duration, action);
    }
    public static STweenState value(Vector3 to, float duration, Action<Vector3> action)
    {
        return Play<ActionVector3, Action<Vector3>, Vector3>(Vector3.zero, to, duration, action);
    }
    public static STweenState value(Vector4 to, float duration, Action<Vector4> action)
    {
        return Play<ActionVector4, Action<Vector4>, Vector4>(Vector4.zero, to, duration, action);
    }
    public static STweenState value(Quaternion to, float duration, Action<Quaternion> action)
    {
        return Play<ActionQuaternion, Action<Quaternion>, Quaternion>(Quaternion.identity, to, duration, action);
    }
    public static STweenState value(Color to, float duration, Action<Color> action)
    {
        return Play<ActionColor, Action<Color>, Color>(new Color(0, 0, 0, 0), to, duration, action);
    }
    public static STweenState value(Bounds to, float duration, Action<Bounds> action)
    {
        return Play<ActionBounds, Action<Bounds>, Bounds>(new Bounds(Vector3.zero, Vector3.zero), to, duration, action);
    }
    public static STweenState value(Rect to, float duration, Action<Rect> action)
    {
        return Play<ActionRect, Action<Rect>, Rect>(new Rect(0, 0, 0, 0), to, duration, action);
    }
    public static STweenState value(RectOffset to, float duration, Action<RectOffset> action)
    {
        return Play<ActionRectOffset, Action<RectOffset>, RectOffset>(new RectOffset(0, 0, 0, 0), to, duration, action);
    }
}
