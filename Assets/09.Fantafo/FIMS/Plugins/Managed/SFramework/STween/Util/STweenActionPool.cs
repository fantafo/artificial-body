﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SFramework.TweenAction;
using System;

namespace SFramework
{
    public class STweenActionPool : ObjectPool<BaseTweenAction>
    {
        Type _type;

        public STweenActionPool(Type type, int initializeCount, int increaseCount)
        {
            _type = type;
            base.initializeCount = initializeCount;
            base.increaseCount = increaseCount;
            base.useWarning = false;
        }

        protected override BaseTweenAction OnCreate()
        {
            var action = (BaseTweenAction)Activator.CreateInstance(_type);
            action._type = _type;
            return action;
        }

        protected override void OnTake(BaseTweenAction action)
        {
            action.Reset();
        }
    }
}
