﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenScrollRectExtension
{
    private static readonly Action<ScrollRect, float> _changeScrollRectDecelerationRate = __changeScrollRectDecelerationRate;
    private static void __changeScrollRectDecelerationRate(ScrollRect scrollRect, float val) { scrollRect.decelerationRate = val; }
    public static STweenState twnDecelerationRate(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.decelerationRate, to, duration, _changeScrollRectDecelerationRate);
    }
    public static STweenState twnDecelerationRate(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectDecelerationRate);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectElasticity = __changeScrollRectElasticity;
    private static void __changeScrollRectElasticity(ScrollRect scrollRect, float val) { scrollRect.elasticity = val; }
    public static STweenState twnElasticity(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.elasticity, to, duration, _changeScrollRectElasticity);
    }
    public static STweenState twnElasticity(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectElasticity);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectHorizontalNormalizedPosition = __changeScrollRectHorizontalNormalizedPosition;
    private static void __changeScrollRectHorizontalNormalizedPosition(ScrollRect scrollRect, float val) { scrollRect.horizontalNormalizedPosition = val; }
    public static STweenState twnHorizontalNormalizedPosition(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.horizontalNormalizedPosition, to, duration, _changeScrollRectHorizontalNormalizedPosition);
    }
    public static STweenState twnHorizontalNormalizedPosition(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectHorizontalNormalizedPosition);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectHorizontalScrollbarSpacing = __changeScrollRectHorizontalScrollbarSpacing;
    private static void __changeScrollRectHorizontalScrollbarSpacing(ScrollRect scrollRect, float val) { scrollRect.horizontalScrollbarSpacing = val; }
    public static STweenState twnHorizontalScrollbarSpacing(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.horizontalScrollbarSpacing, to, duration, _changeScrollRectHorizontalScrollbarSpacing);
    }
    public static STweenState twnHorizontalScrollbarSpacing(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectHorizontalScrollbarSpacing);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectScrollSensitivity = __changeScrollRectScrollSensitivity;
    private static void __changeScrollRectScrollSensitivity(ScrollRect scrollRect, float val) { scrollRect.scrollSensitivity = val; }
    public static STweenState twnScrollSensitivity(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.scrollSensitivity, to, duration, _changeScrollRectScrollSensitivity);
    }
    public static STweenState twnScrollSensitivity(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectScrollSensitivity);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectVerticalNormalizedPosition = __changeScrollRectVerticalNormalizedPosition;
    private static void __changeScrollRectVerticalNormalizedPosition(ScrollRect scrollRect, float val) { scrollRect.verticalNormalizedPosition = val; }
    public static STweenState twnVerticalNormalizedPosition(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.verticalNormalizedPosition, to, duration, _changeScrollRectVerticalNormalizedPosition);
    }
    public static STweenState twnVerticalNormalizedPosition(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectVerticalNormalizedPosition);
    }


    private static readonly Action<ScrollRect, float> _changeScrollRectVerticalScrollbarSpacing = __changeScrollRectVerticalScrollbarSpacing;
    private static void __changeScrollRectVerticalScrollbarSpacing(ScrollRect scrollRect, float val) { scrollRect.verticalScrollbarSpacing = val; }
    public static STweenState twnVerticalScrollbarSpacing(this ScrollRect scrollRect, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, scrollRect.verticalScrollbarSpacing, to, duration, _changeScrollRectVerticalScrollbarSpacing);
    }
    public static STweenState twnVerticalScrollbarSpacing(this ScrollRect scrollRect, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<ScrollRect>, ScrollRect, float>(scrollRect, from, to, duration, _changeScrollRectVerticalScrollbarSpacing);
    }


    private static readonly Action<ScrollRect, Vector2> _changeScrollRectNormalizedPosition = __changeScrollRectNormalizedPosition;
    private static void __changeScrollRectNormalizedPosition(ScrollRect scrollRect, Vector2 val) { scrollRect.normalizedPosition = val; }
    public static STweenState twnNormalizedPosition(this ScrollRect scrollRect, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<ScrollRect>, ScrollRect, Vector2>(scrollRect, scrollRect.normalizedPosition, to, duration, _changeScrollRectNormalizedPosition);
    }
    public static STweenState twnNormalizedPosition(this ScrollRect scrollRect, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<ScrollRect>, ScrollRect, Vector2>(scrollRect, from, to, duration, _changeScrollRectNormalizedPosition);
    }


    private static readonly Action<ScrollRect, Vector2> _changeScrollRectVelocity = __changeScrollRectVelocity;
    private static void __changeScrollRectVelocity(ScrollRect scrollRect, Vector2 val) { scrollRect.velocity = val; }
    public static STweenState twnVelocity(this ScrollRect scrollRect, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<ScrollRect>, ScrollRect, Vector2>(scrollRect, scrollRect.velocity, to, duration, _changeScrollRectVelocity);
    }
    public static STweenState twnVelocity(this ScrollRect scrollRect, Vector2 from, Vector2 to, float duration)
    {
        return STween.Play<ActionVector2Object<ScrollRect>, ScrollRect, Vector2>(scrollRect, from, to, duration, _changeScrollRectVelocity);
    }

}
#endif