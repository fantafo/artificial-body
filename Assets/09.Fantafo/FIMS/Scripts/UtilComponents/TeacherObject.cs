﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 유저가 선생님(방장)일경우에만 활성화되는 오브젝트를 태그한다.
/// 만약 유저가 선생님이 아닐경우에는 지워지거나 deactive된다.
/// 만약 옵저버 플레이어일 경우에는 객체를 무조건 지우지 않고, TeacherObjectController가 관리한다.
/// </summary>
public class TeacherObject : SMonoBehaviour
{
    public bool destroy = true;
    bool active = false;

#if FTF_OBSERVER
    private void Awake()
    {
        TeacherObjectController.main.objs.Add(this);
    }
    private void OnDestroy()
    {
        TeacherObjectController.main.objs.Remove(this);
    }
#endif

    private void OnEnable()
    {
        active = true;
#if FTF_OBSERVER
        destroy = false;
#endif
        Check(destroy);
    }

    private void OnDisable()
    {
        active = false;
    }

    private void Update()
    {
        Check(false);
    }

    public void Check(bool useDestroy)
    {
        if (PlayerInstance.lookMain != null)
        {
            var beforeActive = active;
            gameObject.SetActive(PlayerInstance.lookMain.IsRoomMaster && active);
            active = beforeActive;

            if (useDestroy && !PlayerInstance.lookMain.IsRoomMaster)
            {
                Destroy(gameObject);
            }
        }
    }
}
