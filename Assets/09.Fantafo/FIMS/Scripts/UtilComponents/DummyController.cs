using UnityEngine;
using System.Collections;
using FTF;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class DummyController : SMonoBehaviour
{
    public PlayerInfo info;

    public Color touchPadColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1);
    public Color appButtonColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1);
    public Color systemButtonColor = new Color(20f / 255f, 20f / 255f, 20f / 255f, 1);
    public Vector4 controllerAlphaData = new Vector4(1, 0, 0, 0);
    public Vector4 controllerTouchData = new Vector4(0, 0, 0, 0);
    private Renderer controllerRenderer;
    private MaterialPropertyBlock materialPropertyBlock;

    void Awake()
    {
        Initialize();
    }

    void OnValidate()
    {
        Initialize();
    }

    void Initialize()
    {
        if (controllerRenderer == null)
        {
            controllerRenderer = GetComponent<Renderer>();
        }
        if (materialPropertyBlock == null)
        {
            materialPropertyBlock = new MaterialPropertyBlock();
        }

        materialPropertyBlock.SetColor("_GvrAppButtonColor", appButtonColor);
        materialPropertyBlock.SetColor("_GvrSystemButtonColor", systemButtonColor);
        materialPropertyBlock.SetColor("_GvrTouchInfo", controllerTouchData);
        materialPropertyBlock.SetColor("_GvrTouchPadColor", touchPadColor);
        materialPropertyBlock.SetVector("_GvrControllerAlpha", controllerAlphaData);
        controllerRenderer.SetPropertyBlock(materialPropertyBlock);
    }
}
