﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 클라이언트에게 보낼 커맨드 작성 버퍼.
    /// 실제 커맨드를 보낼 패킷을 제작할 때 사용합니다.
    /// 내부 ObjectPool을 이용하여 Memory부하를 줄입니다.
    /// </summary>
    public class Command
    {
        //================================================================================================
        //
        //                                  Static Variables - Object Pool
        //
        //================================================================================================

        // 오브젝트 풀에 사용될 컬렉션
        static Stack<Command> commands = new Stack<Command>();

        // 초기화
        static Command()
        {
            for (int i = 0; i < 10; i++)
                commands.Push(new Command());
        }

        /// <summary>
        /// 비활성화된 커맨드를 가져옵니다.
        /// </summary>
        public static Command Take()
        {
            lock (commands)
            {
                if (commands.Count > 0)
                {
                    var cmd = commands.Pop();
                    cmd.Reset();
                    return cmd;
                }
                else
                {
                    return new Command();
                }
            }
        }

        /// <summary>
        /// 나 자신을 제외한 다른 유저 모두에게 심플 커맨드(2byte)를 전송합니다.
        /// </summary>
        public static void Broadcast(byte opcode, byte type)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd[1] = type;
            cmd.Broadcast();
            BytePool.Release(cmd);
        }

        /// <summary>
        /// 나를 포함한 모든 유저에게 심플 커맨드(2byte)를 전송합니다.
        /// </summary>
        public static void BroadcastAll(byte opcode, byte type)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd[1] = type;
            cmd.BroadcastAll();
            BytePool.Release(cmd);
        }



        //================================================================================================
        //
        //                                        Member Variables
        //
        //================================================================================================

        // 버퍼
        byte[] buffer;
        MemoryStream mems;
        BinaryWriter writer;

        // 길이
        public int Length { get { return (int)mems.Length; } }
        // 실제 버퍼
        public byte[] Buffer { get { return buffer; } }
        // 실제 BinaryWriter
        public BinaryWriter GetWriter() { return writer; }


        // 생성자
        public Command(int capacity = 4096)
        {
            buffer = new byte[capacity];
            mems = new MemoryStream(buffer, true);
            writer = new BinaryWriter(mems);
        }

        // 초기화
        private void Reset()
        {
            mems.SetLength(0);
        }

        // 폴에 다시 저장한다.
        public void Release()
        {
            lock (commands)
            {
                commands.Push(this);
            }
        }


        //================================================================================================
        //
        //                                        Member Variables
        //
        //================================================================================================

        public void WriteC(byte v) { writer.WriteC(v); }
        public void WriteC(int v) { writer.WriteC(v); }
        public void WriteH(short v) { writer.WriteH(v); }
        public void WriteH(int v) { writer.WriteH(v); }
        public void WriteD(int v) { writer.WriteD(v); }
        public void WriteF(float v) { writer.WriteF(v); }
        public void WriteS(string v) { writer.WriteS(v); }
        public void WriteB(bool v) { writer.WriteB(v); }
        public void WriteV3(Vector3 v) { writer.WriteV3(v); }
        public void WriteV2(Vector2 v) { writer.WriteV2(v); }
        public void WriteQ(Quaternion v) { writer.WriteQ(v); }
        public void WriteRect(Rect v) { writer.WriteRect(v); }
        public void WriteBounds(Bounds v) { writer.WriteBounds(v); }
        public void Write(byte[] v) { writer.Write(v); }
        public void Write(uint v) { writer.Write(v); }

        public void WriteC<T>(T v)
            where T : struct, IConvertible
        {
            writer.WriteC(v.ToByte(null));
        }
    }

    public static class CommandSenderExtension
    {
        public static void Send(this PlayerInstance player, Command cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Command(cmd, player));
            if (autoRelease)
                cmd.Release();
        }
        public static void Send(this PlayerInstance player, IList<Command> cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Command(cmd, player));
            if (autoRelease)
                for (int i = 0; i < cmd.Count; i++)
                    cmd[i].Release();
        }
        public static void Send(this IList<PlayerInstance> player, Command cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Command(cmd, player));
            if (autoRelease)
                cmd.Release();
        }
        public static void Send(this IList<PlayerInstance> player, IList<Command> cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Command(cmd, player));
            if (autoRelease)
                for (int i = 0; i < cmd.Count; i++)
                    cmd[i].Release();
        }

        /// <summary>
        /// 나를 제외한 전체 유저에게 보낸다.
        /// </summary>
        public static void Broadcast(this Command cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Broadcast(cmd));
            if (autoRelease)
                cmd.Release();
        }
        /// <summary>
        /// 나를 제외한 전체 유저에게 보낸다.
        /// </summary>
        public static void Broadcast(this IList<Command> cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.Broadcast(cmd));
            if (autoRelease)
                for (int i = 0; i < cmd.Count; i++)
                    cmd[i].Release();
        }
        /// <summary>
        /// 나를 포함한 전체 유저에게 보낸다.
        /// </summary>
        public static void BroadcastAll(this Command cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.BroadcastAll(cmd));
            if (autoRelease)
                cmd.Release();
        }
        /// <summary>
        /// 나를 포함한 전체 유저에게 보낸다.
        /// </summary>
        public static void BroadcastAll(this IList<Command> cmd, bool autoRelease = true)
        {
            Networker.Send(C_Command.BroadcastAll(cmd));
            if (autoRelease)
                for (int i = 0; i < cmd.Count; i++)
                    cmd[i].Release();
        }





        public static void Send(this PlayerInstance player, byte[] cmd)
        {
            Networker.Send(C_Command.Command(cmd, player));
        }
        public static void Send(this IList<PlayerInstance> player, byte[] cmd)
        {
            Networker.Send(C_Command.Command(cmd, player));
        }
        public static void Broadcast(this byte[] cmd)
        {
            Networker.Send(C_Command.Broadcast(cmd));
        }
        public static void BroadcastAll(this byte[] cmd)
        {
            Networker.Send(C_Command.BroadcastAll(cmd));
        }

        public static void Send(this PlayerInstance player, byte opcode)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            player.Send(cmd);
            BytePool.Release(cmd);
        }
        public static void Send(this IList<PlayerInstance> player, byte opcode)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            player.Send(cmd);
            BytePool.Release(cmd);
        }
        public static void Broadcast(this byte opcode)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd.Broadcast();
            BytePool.Release(cmd);
        }
        public static void BroadcastAll(this byte opcode)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd.BroadcastAll();
            BytePool.Release(cmd);
        }
        public static void Send(this PlayerInstance player, byte opcode, byte type)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd[1] = type;
            player.Send(cmd);
            BytePool.Release(cmd);
        }
        public static void Send(this IList<PlayerInstance> player, byte opcode, byte type)
        {
            byte[] cmd = BytePool.Take(1);
            cmd[0] = opcode;
            cmd[1] = type;
            player.Send(cmd);
            BytePool.Release(cmd);
        }
    }
}
