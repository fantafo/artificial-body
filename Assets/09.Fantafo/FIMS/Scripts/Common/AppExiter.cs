﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;


/// <summary>
/// 어플을 종료할 때 사용합니다.
/// 어플이 Awake도 거치지 않거나 거치는중 종료루틴을 밟게될 경우,
/// VR 플러그인쪽에서 충돌이나서 에러가 발생하는경우가 잦아서 만들어졌습니다.
/// </summary>
public class AppExiter : SMonoBehaviour
{
    static bool shouldExit;
    static string exitMessage;
    public static void Exit(string reason = "unkown")
    {
        if(!shouldExit)
        {
            shouldExit = true;
            exitMessage = reason;
        }
        SLog.Error("AppExiter", "Exit - " + reason);
    }

    int startFrame;

    private void Update()
    {
        if(shouldExit)
        {
            SLog.Error("AppExiter", "Force Quit");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if(focus)
        {
            startFrame = Time.frameCount;
        }
        else
        {
            startFrame = int.MaxValue;
        }
    }
}
