using UnityEngine;

public class DummyGearController : AbsDummyController
{
    const float VALUE = 0.4f;
    const float ENOUGH = 1 - VALUE;

    public Color emissionColor;

    public MeshRenderer touchpad;
    public MeshRenderer touchPoint;
    public MeshRenderer backButton;
    public MeshRenderer systemButton;

    int emissionID;

    public override float BackButtonPress
    {
        get { return backButton.material.GetColor(emissionID).a; }
        set { backButton.material.SetColor(emissionID, (emissionColor * value).SetAlpha(value)); }
    }
    public override float SystemButtonPress
    {
        get { return systemButton.material.GetColor(emissionID).a; }
        set { systemButton.material.SetColor(emissionID, (emissionColor * value).SetAlpha(value)); }
    }

    public override float TouchPointX
    {
        get { return touchPoint.transform.localPosition.x; }
        set { touchPoint.transform.localPosition = touchPoint.transform.localPosition.SetX(value); }
    }
    public override float TouchPointY
    {
        get { return touchPoint.transform.localPosition.y; }
        set { touchPoint.transform.localPosition = touchPoint.transform.localPosition.SetY(value); }
    }
    public override float TouchPointAlpha
    {
        get { return Mathf.Clamp01((touchPoint.material.color.r - 0.3f) / 0.7F); }
        set { touchPoint.material.color = Color.white * (0.7f * value); }
    }
    public override float TouchpadAlpha
    {
        get { return touchpad.material.GetColor(emissionID).a; }
        set { touchpad.material.SetColor(emissionID, (emissionColor * value).SetAlpha(value)); }
    }

    protected override void Initialize()
    {
        emissionID = Shader.PropertyToID("_EmissionColor");
    }

    public override void Validate()
    {
    }
}

