﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.EventSystems;

namespace FTF
{
    /// <summary>
    /// 컨트롤러 레티클의 위치를 가져올 수 있다.
    /// </summary>
    public class FTFReticlePosition : SMonoBehaviour
    {
        public static bool IsInitialized { get; private set; }
        public static Vector3 Value;

        private void OnEnable()
        {
            IsInitialized = true;
        }
        private void OnDisable()
        {
            IsInitialized = false;
        }
        private void LateUpdate()
        {
            Value = transform.position;
        }
    }
}