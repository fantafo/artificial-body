﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.VR;

public class FTFReticlePvrPointer : Pvr_UIPointer
{
    public static bool Click = false;

    [SerializeField]
    private Image m_Selection; // Radial Image

    private Coroutine m_SelectionFillRoutine; // 입력에 따라 Radial 코루틴을 시작하고 중지하는 데 사용됨.
    private bool m_IsSelectionRadialActive = false; // 현재 Radial 진행 중? Whether or not the bar is currently useable.
    private bool m_RadialFilled = false; // Radial이 끝났는지? Used to allow the coroutine to wait for the bar to fill.

    [SerializeField]
    private float m_SelectionDuration; // 라디얼 셀렉션 대기 시간

    private GameObject m_target;    //현재 타깃 대상

    private Ray ray;

    protected override void Awake()
    {
        base.Awake();
        ray = new Ray();
        //Debug.Log("HIDE1");
        Hide();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    private void Update()
    {
        //pico는 메인카메라가 2개라서 Radial를 가운데에 놓기 위해서
        //this.transform.parent.localRotation = Quaternion.Euler(Pvr_UnitySDKManager.SDK.HeadPose.Orientation.eulerAngles.x, Pvr_UnitySDKManager.SDK.HeadPose.Orientation.eulerAngles.y, 0);
       

        if (m_target == null || m_target.activeInHierarchy == false)
        {

            //Debug.Log("HIDE2 " + m_target);
            HandleUp();
            Hide();
        }


        ray.direction = this.transform.forward;
        ray.origin = this.transform.position;

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (m_target != null && m_target == hit.transform.gameObject)
                return;

            if (1 << hit.transform.gameObject.layer == LayerMask.GetMask("Coin"))
            {
                m_target = hit.transform.gameObject;
                Show();
                HandleDown(m_target);
            }
        }
        else
        {
            m_target = null;
            HandleUp();
        }
    }

    public override void OnUIPointerElementEnter(UIPointerEventArgs e)
    {
        base.OnUIPointerElementEnter(e);
        //Debug.Log("OnUIPointerElementEnter " + e.currentTarget.name);
        //방장이 관광지 선택후 UICANVAS_DRAGGABLE_PANEL이 선택되서 오류 발생.
        //var target = ExecuteEvents.ExecuteHierarchy(e.raycastResult.gameObject, this.pointerEventData, ExecuteEvents.pointerDownHandler);
        var target = e.currentTarget;
        if (target != null && target.name == "UICANVAS_DRAGGABLE_PANEL")
        {
            HandleUp();
            return;
        }
    
        m_target = target;
        Show();
        HandleDown(e);
    }

    public override void OnUIPointerElementExit(UIPointerEventArgs e)
    {
        //Debug.Log("OnUIPointerElementExit " + e.currentTarget);
        base.OnUIPointerElementExit(e);
        m_target = null;
        HandleUp();
        Hide();
    }

    public override void OnUIPointerElementClick(UIPointerEventArgs e)
    {
        base.OnUIPointerElementClick(e);

    }

    protected override void ConfigureEventSystem()
    {
        //EventSystem가 두개 이상일때 문제발생. 수정
        if (!cachedEventSystem)
        {
            //cachedEventSystem = FindObjectOfType<EventSystem>();
            EventSystem[] events = FindObjectsOfType<EventSystem>();
            for (int i = 0; i < events.Length; ++i)
            {
                if (events[i].GetComponent<Pvr_InputModule>())
                {
                    cachedEventSystem = events[i];
                    break;
                }
            }
        }

        if (!cachedVRInputModule)
        {
            cachedVRInputModule = cachedEventSystem.GetComponent<Pvr_InputModule>();
        }

        if (cachedEventSystem && cachedVRInputModule)
        {
            if (pointerEventData == null)
            {
                pointerEventData = new PointerEventData(cachedEventSystem);
            }

            if (!cachedVRInputModule.pointers.Contains(this))
            {
                cachedVRInputModule.pointers.Add(this);
            }
        }
    }

    // Radial Image 보이기
    // Radial Action 시작
    public void Show()
    {
        m_Selection.fillAmount = 0f;
        m_Selection.gameObject.SetActive(true);
        m_IsSelectionRadialActive = true;
    }

    public void Hide()
    {
        m_Selection.gameObject.SetActive(false);
        m_IsSelectionRadialActive = false;

        // This effectively resets the radial for when it's shown again.
        m_Selection.fillAmount = 0f;
    }

    // VRInput OnDown : Radial Bar 채워지기 시작
    public void HandleDown(UIPointerEventArgs e)
    {
        // If the radial is active start filling it.
        if (m_IsSelectionRadialActive)
        {
            //자주 HandleUp()이 실행 안되는 경우 있어서 시작전 코루틴 정지
            if (m_SelectionFillRoutine != null)
                StopCoroutine(m_SelectionFillRoutine);

            m_SelectionFillRoutine = StartCoroutine(FillSelectionRadial(e));
        }
    }

    public void HandleDown(GameObject target)
    {
        // If the radial is active start filling it.
        if (m_IsSelectionRadialActive)
        {
            //자주 HandleUp()이 실행 안되는 경우 있어서 시작전 코루틴 정지
            if (m_SelectionFillRoutine != null)
                StopCoroutine(m_SelectionFillRoutine);

            m_SelectionFillRoutine = StartCoroutine(FillSelectionRadial(target));
        }
    }

    // VRInput OnUp : Radial Bar 취소.
    public void HandleUp()
    {
        // If the radial is active stop filling it and reset it's amount.
        if (m_IsSelectionRadialActive)
        {
            if (m_SelectionFillRoutine != null)
            {
                StopCoroutine(m_SelectionFillRoutine);
            }

            m_Selection.fillAmount = 0f;
        }
    }

    // Radial Action 함수 (채우기)
    private IEnumerator FillSelectionRadial(UIPointerEventArgs e)
    {
        // At the start of the coroutine, the bar is not filled.
        m_RadialFilled = false;

        // Create a timer and reset the fill amount.
        float timer = 0f;
        m_Selection.fillAmount = 0f;

        // This loop is executed once per frame until the timer exceeds the duration.
        // 이 루프는 타이머가 지속 시간을 초과 할 때까지 프레임 당 한 번 실행.
        while (timer < m_SelectionDuration)
        {
            // The image's fill amount requires a value from 0 to 1 so we normalise the time.
            m_Selection.fillAmount = timer / m_SelectionDuration;

            // Increase the timer by the time between frames and wait for the next frame.
            timer += Time.deltaTime;
            //Debug.Log(timer + " , " + m_SelectionDuration);
            yield return null;
        }

        // When the loop is finished set the fill amount to be full.
        // 다 찼다!
        m_Selection.fillAmount = 1f;

        // Turn off the radial so it can only be used once.
        // 작업 끝
        m_IsSelectionRadialActive = false;

        // The radial is now filled so the coroutine waiting for it can continue.
        // 다 채움
        m_RadialFilled = true;

        // If there is anything subscribed to OnSelectionComplete call it.
        // 다 채우고 난 후 아래 이벤트와 연결된 함수 호출
        //pico 클릭 호출
        var target = ExecuteEvents.ExecuteHierarchy(e.raycastResult.gameObject, this.pointerEventData, ExecuteEvents.pointerDownHandler);
        if (target != null)
        {
            this.pointerEventData.pressPosition = this.pointerEventData.position;
            this.pointerEventData.pointerPressRaycast = e.raycastResult;
            this.pointerEventData.pointerPress = target;
        }
        /* 기존 샘플코드
        if (OnSelectionComplete != null)
            OnSelectionComplete();
            */
        yield return null;
    }

    // Radial Action 함수 (채우기)
    private IEnumerator FillSelectionRadial(GameObject target)
    {
        //At the start of the coroutine, the bar is not filled.
        m_RadialFilled = false;

        // Create a timer and reset the fill amount.
        float timer = 0f;
        m_Selection.fillAmount = 0f;

        // This loop is executed once per frame until the timer exceeds the duration.
        // 이 루프는 타이머가 지속 시간을 초과 할 때까지 프레임 당 한 번 실행.
        while (timer < m_SelectionDuration)
        {
            // The image's fill amount requires a value from 0 to 1 so we normalise the time.
            m_Selection.fillAmount = timer / m_SelectionDuration;

            // Increase the timer by the time between frames and wait for the next frame.
            timer += Time.deltaTime;
            yield return null;
        }

        // When the loop is finished set the fill amount to be full.
        // 다 찼다!
        m_Selection.fillAmount = 1f;

        // Turn off the radial so it can only be used once.
        // 작업 끝
        m_IsSelectionRadialActive = false;

        // The radial is now filled so the coroutine waiting for it can continue.
        // 다 채움
        m_RadialFilled = true;

        // If there is anything subscribed to OnSelectionComplete call it.
        // 다 채우고 난 후 아래 이벤트와 연결된 함수 호출
        //pico 클릭 호출

        if (target != null)
        {
            //var coinPoint = target.GetComponent<CoinPoint>();
            //if (coinPoint != null)
            //{
            //    coinPoint.Broadcast("Finding", PlayerInstance.main.instanceId, target.name);
            //}

            //var coinPoint2 = target.GetComponent<CoinChanceItem>();
            //if (coinPoint2 != null)
            //{
            //    coinPoint2.OnClick();
            //}

            //var ItemPoint = target.GetComponent<HiddenFinderItem>();
            //if (ItemPoint != null)
            //{
            //    ItemPoint.OnClick();
            //}
        }
        /* 기존 샘플코드
        if (OnSelectionComplete != null)
            OnSelectionComplete();
            */
        yield return null;
    }
}
