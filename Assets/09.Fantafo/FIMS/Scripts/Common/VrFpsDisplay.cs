﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VrFpsDisplay : MonoBehaviour {

    private const string DISPLAY_TEXT_FORMAT = "{0:0.0} msf\n({1:0} FPS)";
    private const float MS_PER_SEC = 1000f;

    private string fpsText;
    private float fps = 60;

    public Text text;

    void LateUpdate ()
    {
        float deltaTime = Time.unscaledDeltaTime;
        float interp = deltaTime / (0.5f + deltaTime);
        float currentFPS = 1.0f / deltaTime;
        fps = Mathf.Lerp(fps, currentFPS, interp);
        float msf = MS_PER_SEC / fps;
        fpsText = string.Format(DISPLAY_TEXT_FORMAT, msf, fps);

        text.text = fpsText;
    }
}
