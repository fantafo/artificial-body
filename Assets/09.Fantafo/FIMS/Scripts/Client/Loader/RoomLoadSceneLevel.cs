﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    [RequireComponent(typeof(RoomInfo))]
    [Description("빌드 세팅에 있는 씬번호를 이용해서 맵을 읽어옵니다. 0번은 무조건 ReadyScene이여야하며, 1번 부터 맵번호로 인식한다.")]
    public class RoomLoadSceneLevel : RoomLoadBase
    {
        protected override AsyncOperation LoadAsync()
        {
            Debug.Log(GetType() + "/ " + room.mapID);
            FimsCommonData.PushFlag(FimsCommonData.FLAG_LOADING);
            return SceneManager.LoadSceneAsync(room.mapID, LoadSceneMode.Single);
        }

        [ContextMenu("Load")]
        protected override void LoadStart()
        {
            base.LoadStart();
        }

        protected override bool LoadValidate()
        {
            return true;
        }
    }
}