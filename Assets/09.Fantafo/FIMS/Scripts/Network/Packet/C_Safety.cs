﻿using System;

namespace FTF.Packet
{
    class C_Safety
    {
        /*해당 콘텐츠가 종료 됨을 전달.
         * index는 현재 진행한 콘텐츠 인덱스
        다른 유저가 콘텐츠가 진행중이라면 최대 ms밀리초 만큼 기다립니다.*/
        public static byte[] ContentsEnd(short index, int ms)
        {
            SLog.Debug("ContentsEnd", index);
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Safety_Contents_End);
            w.WriteH(index);
            w.WriteD(ms);
            return w.ToBytes();
        }

        /*해당 SummaryQuiz중 한문제가 종료 됨을 전달.
         * index는 현재 진행한 퀴즈 인덱스
        다른 유저가 콘텐츠가 진행중이라면 최대 ms밀리초 만큼 기다립니다.*/
        public static byte[] QuizEnd(short index, int ms)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Safety_Quiz_End);
            w.WriteH(index);
            w.WriteD(ms);
            return w.ToBytes();
        }

        /*해당 MapStage의 세부 정보를 요청한다.*/
        public static byte[] RequestDetailMapStage()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Safety_Request_DetailMapStage);
            return w.ToBytes();
        }

        public static byte[] Start()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Safety_Start);
            return w.ToBytes();
        }
    }
}
