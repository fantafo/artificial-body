﻿using System;

namespace FTF.Packet
{
    /// <summary>
    /// 방에 접속한 뒤, 방장의 권한, 방 설정 등의 작업을하는 패킷을 모음
    /// </summary>
    public class C_Room
    {
        public static byte[] Exit()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_Exit);
            return w.ToBytes();
        }

        public static byte[] Ban(int instanceID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_Ban);
            w.WriteD(instanceID);
            return w.ToBytes();
        }

        public static byte[] ChangeLevel(short mapID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_ChangeLevel);
            w.WriteH(mapID);

            return w.ToBytes();
        }

        public static byte[] Ready(bool isReady)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_Ready);
            w.WriteB(isReady);
            return w.ToBytes();
        }

        public static byte[] LoadComplete()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_LoadComplete);
            return w.ToBytes();
        }

        public static byte[] MapStage(short mapStage)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_MapStage);
            w.WriteH(mapStage);
            return w.ToBytes();
        }

        public static byte[] RoomEnd()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Room_End);
            return w.ToBytes();
        }
    }
}