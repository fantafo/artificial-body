﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.Packet
{
    public enum Quiz_OX : short
    {
        None = 0,
        True = 1,
        False = 2
    }

    public class Quiz
    {
        public static byte[] OX(Quiz_OX ox)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Quiz_OX);
            w.WriteH((short)ox);
            return w.ToBytes();
        }
    }
}