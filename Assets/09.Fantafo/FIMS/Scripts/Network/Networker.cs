﻿#if UNITY_EDITOR || UNITY_STANDALONE
#define VISIBLEPACKET
#endif

using FreeNet;
using FreeNetUnity;
using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace FTF
{
    public interface IMessageReceiver
    {
        void on_recv(CPacket msg);
    }


    public enum NetworkState
    {
        Disconnect = -1, // Multi
        None = 0,
        Connecting, // Multi
        Connected, // Multi
        Validating, // Multi
        Validated,      //버전확인 패킷 받을때 변경됨
        Loging,
        Logined,
        Channel,
        RoomJoining,
        Room,
        Loading,
        Playing,
    }

    /// <summary>
    /// 서버와 연결하기 위한 클라이언트 소켓을 관리한다.
    /// 서버에 접속, 데이터 읽기, 데이터 쓰기의 IO관리를 실시한다.
    /// 이곳에서는 단순 바이너리 데이터를 관리하고 처리하며,
    /// 논리 처리는 PacketHandler에서 이뤄진다.
    /// 이곳에서 읽어온 데이터는 PacketHandler.Push를 통해서 입력되게 된다.
    /// </summary>
    public class Networker : SMonoBehaviour, IMessageReceiver
    {
        ILog log = SLog.GetLogger("Networker");
        public static Networker main { get; private set; }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Static Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        /// <summary>
        /// 클라이언트의 아이피 주소를 가리킵니다.
        /// </summary>
        public static string ipAddress { get; private set; }

        //접속을 종료원할 경우 true
        public static Boolean bConnectEnd = false;

        /// <summary>
        /// 네트워크의 연결상태 및 클라이언트가 어떤 단계까지 진행했는지에 대해서 알 수 있습니다.
        /// </summary>
        private static NetworkState _state;
        public static NetworkState State { get { return _state; } set { if (_state != value) { if (onChangedNetworkState != null) onChangedNetworkState(_state, value); _state = value; } } }
        public static bool IsConnected { get { return State < NetworkState.Connected; } }

        /// <summary>
        /// 네트워크가 실행된 Id를 가리키며 네트워크 처리중 재접속이 됐을 때, 이전 연결과 다음 연결을 구별하기 위해 사용됩니다.
        /// </summary>
        public static int ConnectionId { get; private set; }

        /// <summary>
        /// 네트워크 상태가 변경됐을 때 호출된다.
        /// </summary>
        public static event Action<NetworkState, NetworkState> onChangedNetworkState;

        public static void Send(byte[] data, bool flush = true)
        {
            if (main != null && State >= NetworkState.Validating)
            {
                main.send(data, flush);
            }
        }

        public static void Disconnect(bool packet)
        {
            if (main != null)
                main.disconnect(packet);
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Member Variables                   //
        //                                                    //
        ////////////////////////////////////////////////////////

        [Header("서버 연결정보")]
        public bool autoConnectOnStart = true;
        public bool autoSendVersionPacket = true;
        [Header("Client Identifier")]
        [Tooltip("방을 생성하거나 검색, 입장할때의 조건이 된다. 이 값이 동일한 대상과만 같은 방을 이룰 수 있다.")]
        public int clientPacketVersion;
        [NonSerialized]
        public string clientIdentifier;
        [Header("Component")]
        public NetworkerPacketHandler packetHandler;

#if UNITY_EDITOR || VISIBLEPACKET
        [Header("VisibleList")]
        public bool visibleSendPacketAll;
        public bool visibleReceivePacketAll;
        public ClientOpcode[] visibleSendPacket;
        public ServerOpcode[] visibleReceivePacket;
        public ClientOpcode[] invisibleSendPacket;
        public ServerOpcode[] invisibleReceivePacket;
#endif

        // 네트워크 관련 정보들
        private Socket _socket;
        private Cipher _cipher;
        public Socket GetSocket()
        {
            return _socket;
        }


        //FreeNet
        Queue<byte[]> sending_queue;
        CFreeNetUnityService freenet;
        public IMessageReceiver message_receiver;


        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Life Cycle                         //
        //                                                    //
        ////////////////////////////////////////////////////////

        private void Awake()
        {
            if (main)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
                return;
            }

            //FreeNet
            this.freenet = gameObject.AddComponent<CFreeNetUnityService>();
            this.freenet.appcallback_on_message += this.on_message;
            this.freenet.appcallback_on_status_changed += this.on_status_changed;

            this.sending_queue = new Queue<byte[]>();

            ipAddress = Network.player.ipAddress;
            State = NetworkState.None;
            ConnectionId = 0;

            clientIdentifier = ConnectInfo.Identifier;

            log.DebugFormat("Initialize Networker ({0}/{1})", clientIdentifier, clientPacketVersion);
            main = this;
            transform.SetParent(null, true);
            DontDestroyOnLoad(gameObject);
        }
        private IEnumerator TStart()
        {
            yield return null; // 다른 컴포넌트들이 세팅될 때까지 기다린다.

            if (autoConnectOnStart)
                connect();
        }
        private void OnEnable()
        {
            log.Debug("OnEnable");
            StartCoroutine(TStart());
        }

        // 게임을 종료시킬때
        private void OnDisable()
        {
            log.Debug("OnDisable");
            disconnect(true);
        }
        private void OnApplicationQuit()
        {
            log.Debug("OnApplicationQuit");
            disconnect(true);
        }

        private void OnDestroy()
        {
            log.Debug("OnDestroy");
            Disconnect(true);
            if (main == this)
            {
                main = null;
                ipAddress = null;
                State = NetworkState.None;
                ConnectionId = 0;

            }
        }


        ////////////////////////////////////////////////////////
        //                                                    //
        //                 Operation Functions                //
        //                                                    //
        ////////////////////////////////////////////////////////

        public void connect()
        {
            if (!this.freenet.is_connected())
            {
                log.Debug("Begin Connecting");

                State = NetworkState.None;

                // 이전에 보내지 못한 패킷은 모두 버린다.
                this.sending_queue.Clear();

                if (!this.freenet.is_connected())
                {
                    log.Debug("freenet Begin Connecting");
                    State = NetworkState.Connecting;
                    this.freenet.connect(ConnectInfo.ConnectIP, ConnectInfo.ConnectPort);
                }
            }
        }

        public void disconnect(bool packet)
        {
            log.Debug("Begin Disconnect");

            // 게임종료 패킷 전송
            if (State >= NetworkState.Validating && packet)
            {
                send(C_Common.Quit());
            }

            this.freenet.disconnect();
        }

        void on_message(CPacket msg)
        {
            //Debug.Log("on_message recv");
            if ((ServerOpcode)msg.buffer[2] == 0)
                return;
            PrintReadPacket(msg.buffer);
            packetHandler.Push(msg.buffer);
            //this.message_receiver.on_recv(msg);
        }

        void on_status_changed(NETWORK_EVENT status)
        {
            switch (status)
            {
                case NETWORK_EVENT.disconnected:
                    Debug.Log("연결 종료..");
                    if (!bConnectEnd)
                    {
                        Debug.Log("연결종료.. 재접속 시작");
                        disconnect(true);
                        connect();
                        //back_to_main();
                    }
                    break;
            }
        }

        ////////////////////////////////////////////////////////
        //                                                    //
        //                  Network                           //
        //                                                    //
        ////////////////////////////////////////////////////////
        /// <summary>
        /// 패킷을 수신 했을 때 호출됨.
        /// 여기서 사용 x
        /// </summary>
        /// <param name="protocol"></param>
        /// <param name="msg"></param>
        void IMessageReceiver.on_recv(CPacket msg)
        {
            Debug.Log("IMessageReceiver recv");
            PrintReadPacket(msg.buffer);
            packetHandler.Push(msg.buffer);
        }

        //[System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void PrintReadPacket(byte[] data)
        {
#if UNITY_EDITOR || VISIBLEPACKET
            if (visibleReceivePacketAll)
            {
                ServerOpcode code = (ServerOpcode)data[2];
                if (invisibleReceivePacket == null || invisibleReceivePacket.IndexOf(code) == -1)
                {
                    Debug.LogFormat("<color=#DD77AA>===[Receive]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                }
            }
            else if (!visibleReceivePacket.IsEmpty())
            {
                ServerOpcode code = (ServerOpcode)data[2];
                foreach (var op in visibleReceivePacket)
                {
                    if (op == code)
                    {
                        Debug.LogFormat("<color=#DD77AA>===[Receive]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                        break;
                    }
                }
            }
#endif
        }

        /// <summary>
        /// 서버에 전달할 패킷을 전송한다.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="flush"></param>
        public void send(byte[] data, bool flush = true)
        {
            if (State < NetworkState.Validating)
                return;

            lock (this)
            {
                //PrintSendPacket(data);
                try
                {
                    short packet_size = (short)(data.Length + sizeof(short));
                    byte[] packet = new byte[packet_size];
                    //패킷사이즈를 byte[]로 변환후 packet에 저장
                    BitConverter.GetBytes(packet_size).CopyTo(packet, 0);
                    data.CopyTo(packet, sizeof(short));
                    this.sending_queue.Enqueue(packet);
                    PrintSendPacket(packet);
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
        }
        //[System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void PrintSendPacket(byte[] data)
        {
            //ClientOpcode code = (ClientOpcode)data[2];
            //Debug.Log("<color=#77AADD>===[Send]=== " + code.ToString() + "(" + (int)code + ") : " + data.Length + "byte\n" + HexString.ToString(data, data.Length));
            //Debug.LogFormat("<color=#77AADD>===[Send]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));

#if UNITY_EDITOR || VISIBLEPACKET
            if (visibleSendPacketAll)
            {
                ClientOpcode code = (ClientOpcode)data[2];
                if (invisibleSendPacket == null || invisibleSendPacket.IndexOf(code) == -1)
                {
                    Debug.Log("<color=#77AADD>===[Send]=== " + code.ToString() + "(" + (int)code + ") : " + data.Length + "byte\n" + HexString.ToString(data, data.Length));
                    Debug.LogFormat("<color=#77AADD>===[Send]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                }
            }
            else if (!visibleSendPacket.IsEmpty())
            {
                ClientOpcode code = (ClientOpcode)data[2];
                foreach (var op in visibleSendPacket)
                {
                    if (op == code)
                    {
                        Debug.Log("<color=#77AADD>===[Send]=== " + code.ToString() + "(" + (int)code + ") : " + data.Length + "byte\n" + HexString.ToString(data, data.Length));
                        Debug.LogFormat("<color=#77AADD>===[Send]=== {0}({1:X2}) : {2}byte\n{3}</color>", code.ToString(), (int)code, data.Length, HexString.ToString(data, data.Length));
                        break;
                    }
                }
            }
#endif
        }

        void Update()
        {
            if (!this.freenet.is_connected())
            {
                return;
            }
            else if (this.freenet.is_connected() && State < NetworkState.Connected)
            {
                State = NetworkState.Connected;
                // 클라이언트 버전 정보 전송
                State = NetworkState.Validating;
                ConnectionId++;

                if (autoSendVersionPacket)
                {
                    send(C_Common.Version(clientIdentifier, clientPacketVersion, false));
                }
            }
            while (this.sending_queue.Count > 0)
            {
                byte[] msg = this.sending_queue.Dequeue();
                this.freenet.send(msg);
            }
        }

        public bool is_connected()
        {
            if (this.freenet == null)
            {
                return false;
            }

            return this.freenet.is_connected();
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                  Editor Network Monitor
        //
        //////////////////////////////////////////////////////////////////////////////////////////
#if UNITY_EDITOR
        [Space]
        [Header("Network Monitor")]
        public int _clearTime = 99999999;
        public int _totalSendPacketSize = 0;
        public int _averageSendPacketSize = 0;
        public int _totalReceivePacketSize = 0;
        public int _averageReceivePacketSize = 0;
        public DateTime _packetStartTime = new DateTime(0);
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;
            else if ((DateTime.Now - _packetStartTime).TotalSeconds > _clearTime)
            {
                _packetStartTime = DateTime.Now;
                _totalSendPacketSize = _totalReceivePacketSize = _averageReceivePacketSize = _averageSendPacketSize = 0;
            }

            _totalSendPacketSize += size + 2;
            _averageSendPacketSize = (int)(_totalSendPacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
        void AddReceiveMonitor(int size)
        {
            if (_packetStartTime.Ticks == 0)
                _packetStartTime = DateTime.Now;
            else if ((DateTime.Now - _packetStartTime).TotalSeconds > _clearTime)
            {
                _packetStartTime = DateTime.Now;
                _totalSendPacketSize = _totalReceivePacketSize = _averageReceivePacketSize = _averageSendPacketSize = 0;
            }

            _totalReceivePacketSize += size + 2;
            _averageReceivePacketSize = (int)(_totalReceivePacketSize / (DateTime.Now - _packetStartTime).TotalSeconds);
        }
#else
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddSendMonitor(int size){}
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        void AddReceiveMonitor(int size){}
#endif
    }
}