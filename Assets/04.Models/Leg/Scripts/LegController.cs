﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegController : MonoBehaviour
{
    public GameObject Leg_Assemble;
    public GameObject Leg_Up;
    public GameObject Leg_Down;

    private int LegNum;

    private bool isChange;

    private bool isLegSeleted;

    private bool isScaleEditing;

    private float Leg_Scale;
    private float Leg_Rotate;
    private float before_Leg_Rotate;

    private float rotationOnce;
    private int AssembleCount;

    private Vector3 UpMove;
    private Vector2 TouchBefore;
    private float initTouchDist;

    public GameObject Effect1;
    public GameObject Effect2;

    public AudioSource Electricsound;

    private bool isFirst;
    public AudioSource TouchSound;

    private bool autoRotationStart;
    // Start is called before the first frame update
    void Start()
    {
        UpMove = new Vector3(0.0f, 0.0f, 0.0f);

        Leg_Assemble.SetActive(true);
        Leg_Up.SetActive(false);
        Leg_Down.SetActive(false);

        isChange = false;
        isLegSeleted = false;
        LegNum = 0;
        Leg_Rotate = 0.0f;
        Leg_Scale = 0.7f;
        isScaleEditing = false;
        before_Leg_Rotate = 0.0f;
        AssembleCount = 0;
        autoRotationStart = false;
        isFirst = true;
    }

    // Update is called once per frame
    void Update()
    {
        Leg_Assemble.transform.rotation = Quaternion.Euler(0.0f, -Leg_Rotate / 5.0f, 0.0f);
        Leg_Up.transform.rotation = Quaternion.Euler(0.0f, -Leg_Rotate / 5.0f, 0.0f);
        Leg_Down.transform.rotation = Quaternion.Euler(0.0f, -Leg_Rotate / 5.0f, 0.0f);

        Leg_Assemble.transform.localScale = new Vector3(Leg_Scale, Leg_Scale, Leg_Scale);
        Leg_Up.transform.localScale = new Vector3(Leg_Scale, Leg_Scale, Leg_Scale);
        Leg_Down.transform.localScale = new Vector3(Leg_Scale, Leg_Scale, Leg_Scale);

        if(autoRotationStart)
        {
            
                Leg_Rotate += Time.deltaTime*360.0f;
                Debug.Log("도나 안 도나 : " + rotationOnce);
          
        }
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                TouchBefore = Input.GetTouch(0).position;

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(TouchBefore);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.tag == "Leg")
                    {
                        isLegSeleted = true;
                        Debug.Log("맞았다!");
                    }
                    else
                    {
                        isLegSeleted = false;
                    }
                }
                else
                {
                    Debug.Log("안맞음 ㅠㅠ!");

                }

            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved && !isScaleEditing)
            {
                Leg_Rotate = (Input.GetTouch(0).position - TouchBefore).x + before_Leg_Rotate;
                isLegSeleted = false;

            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                isScaleEditing = false;
               
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0));
                if (Physics.Raycast(ray, out hit, Mathf.Infinity) && isLegSeleted)
                {

                    if (hit.transform.tag == "Leg")
                    {
                        TouchSound.Play();
                        if (AssembleCount == 0 && LegNum % 3 == 0)
                        {
                            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", true);
                            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
                            Leg_Assemble.GetComponent<Animator>().SetBool("ENdTouch", false);
                            AssembleCount++;
                            rotationOnce = 0.0f;
                            if (isFirst)
                            {
                                isFirst = false;
                                Effect1.SetActive(true);
                                Effect2.SetActive(true);
                                Electricsound.Play();
                            }
                        }
                        else if (AssembleCount == 1 && LegNum % 3 == 0)
                        {
                            autoRotationStart = true;
                            //이펙트 추가 
                            Effect1.SetActive(false);
                            Effect2.SetActive(false);

                            AssembleCount++;
                        }
                        else if (AssembleCount == 2 && LegNum % 3 == 0)
                        {
                            autoRotationStart = false;
                            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", false);
                            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", true);
                            Leg_Assemble.GetComponent<Animator>().SetBool("EndTouch", false);
                            AssembleCount++;
                        }
                        else if (AssembleCount == 3 && LegNum % 3 == 0)
                        {
                            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", true);
                            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
                            Leg_Assemble.GetComponent<Animator>().SetBool("EndTouch", true);
                            AssembleCount=0;

                            LegNum++;
                            ChangeLeg((LegNum) % 3);
                            isLegSeleted = false;
                            Debug.Log("교체!!");
                        }
                        else
                        {
                            LegNum++;
                            ChangeLeg((LegNum) % 3);
                            isLegSeleted = false;
                            Debug.Log("교체!!");
                        }
                    }
                    else
                    {
                        isLegSeleted = false;
                        Debug.Log("취소!!");
                    }
                }
                before_Leg_Rotate = Leg_Rotate;
            }
        }
        else if (Input.touchCount == 2)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Began)
            {
                initTouchDist = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
                isScaleEditing = true;

                isLegSeleted = false;
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                float nowTouchDist = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
                Leg_Scale += (nowTouchDist - initTouchDist) / 1000.0f;
                initTouchDist = nowTouchDist;
                if (Leg_Scale > 2.0)
                    Leg_Scale = 2.0f;
                if (Leg_Scale < 0.5)
                    Leg_Scale = 0.5f;
            }
        }


    }

    public void ChangeLeg(int LegNum)
    {
        if (LegNum == 0)
        {
            Leg_Assemble.SetActive(true);
            Leg_Up.SetActive(false);
            Leg_Down.SetActive(false);
        }
        else if (LegNum == 1)
        {
            Leg_Assemble.SetActive(false);
            Leg_Up.SetActive(true);
            Leg_Down.SetActive(false);
        }
        else if (LegNum == 2)
        {
            Leg_Assemble.SetActive(false);
            Leg_Up.SetActive(false);
            Leg_Down.SetActive(true);
        }
    }
}


