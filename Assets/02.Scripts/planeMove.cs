﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class planeMove : MonoBehaviour {

    [SerializeField]
    private float rotationTime;
    [SerializeField]
    private float rotationX;
    [SerializeField]
    private float rotationZ;

    private Vector3 StartPos;
    private float rotationAngle;

    public GameObject target;


    public float test;

    float startH;
    float targetH;

    public float test2;

    public float test3;
    float randomspeed;
    bool RLFlag;
    System.Random r;
	void Start ()
    {
        if(rotationTime == 0.0f)
        {
            rotationTime = 10.0f;
        }
        StartPos = gameObject.transform.position;
        RLFlag = true;
        rotationAngle = 0.0f;
        randomspeed = 0.0f;
        rotationAngle = test2;
    }
	
	// Update is called once per frame
	void Update ()
    {
        rotationAngle += Time.deltaTime / rotationTime ;
   

        gameObject.transform.position = StartPos + new Vector3(Mathf.Sin(rotationAngle ) * rotationX*test + test3, 0.0f, Mathf.Cos(rotationAngle/2.0f) * rotationZ/test + test3) ;
        target.transform.position = StartPos + new Vector3(Mathf.Sin(rotationAngle+0.01f) * rotationX*test + test3, 0.0f, Mathf.Cos((rotationAngle + 0.01f) / 2.0f) * rotationZ/test + test3);


        Vector3 direction = target.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = rotation;
        transform.Rotate(new Vector3(90.0f, 0, 0));
    }
}
