﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;


public class HandClick : AutoCommander
{
    public GameObject Hand_Assemble;
    public GameObject Hand_ZamZam;

    public AudioSource buttonSound;
    public AudioSource electricSound;

    public GameObject Effect_one;
    public GameObject Effect_two;

    private bool isFirst = true;
    private int clickCount = 0;
    private bool rotationFlag = false;

    // Use this for initialization
    void Start ()
    {
       // Hand_ZamZam.GetComponent<Animator>().Play()
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (rotationFlag)
        {
            gameObject.transform.Rotate(new Vector3(0, 1f, 0));
            //Hand_ZamZam.transform.Rotate(new Vector3(0, 1f, 0));
        }
    }
    public void HandClickPunc()
    {
        clickCount++;
        clickCount = clickCount % 6;
        Broadcast("HandClickCommand", clickCount);
        Debug.Log("테스트텟스트");
    }

    [OnCommand]
    public void HandClickCommand(int count)
    {
        buttonSound.Play();
        if (isFirst)
        {

            isFirst = false;
            Effect_one.SetActive(true);
            Effect_two.SetActive(true);
            electricSound.Play();
            Effect_one.GetComponent<ParticleSystem>().Play();
            Effect_two.GetComponent<ParticleSystem>().Play();
        }
        else
        {

            if (electricSound.isPlaying)
            {
                electricSound.Stop();
            }
            if (Effect_one.GetComponent<ParticleSystem>().isPlaying)
            {
                Effect_one.GetComponent<ParticleSystem>().Stop();
            }
            if (Effect_two.GetComponent<ParticleSystem>().isPlaying)
            {
                Effect_two.GetComponent<ParticleSystem>().Stop();
            }
        }


        if (count == 0)
        {
            Hand_Assemble.SetActive(true);
            Hand_ZamZam.SetActive(false);
        }
        else if (count == 1)
        {
            Hand_Assemble.GetComponent<Animator>().SetBool("FirstTouch", true);
            Hand_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
            Hand_Assemble.GetComponent<Animator>().SetBool("EndTouch", false);
        }
        else if (count == 2)
        {
            rotationFlag = true;
        }
        else if (count == 3)
        {
            rotationFlag = false;
        }
        else if (count == 4)
        {
            Hand_Assemble.GetComponent<Animator>().SetBool("FirstTouch", false);
            Hand_Assemble.GetComponent<Animator>().SetBool("AfterTouch", true);
            Hand_Assemble.GetComponent<Animator>().SetBool("EndTouch", false);
        }
        else if (count == 5)
        {
            Hand_Assemble.GetComponent<Animator>().SetBool("FirstTouch", false);
            Hand_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
            Hand_Assemble.GetComponent<Animator>().SetBool("EndTouch", true);

            Hand_Assemble.SetActive(false);
            Hand_ZamZam.SetActive(true);
        }
    }
}
