﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;

public class LegClick : AutoCommander
{
    public GameObject Leg_Assemble;
    public GameObject Leg_Up;
    public GameObject Leg_Down;

    public AudioSource buttonSound;
    public AudioSource electricSound;

    public GameObject Effect_one;
    public GameObject Effect_two;

    private bool isFirst = true;
    private int clickCount = 0;
    private bool rotationFlag = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (rotationFlag)
        {
            Leg_Assemble.transform.Rotate(new Vector3(0, 1f, 0));
            Leg_Up.transform.Rotate(new Vector3(0, 1f, 0));
            Leg_Down.transform.Rotate(new Vector3(0, 1f, 0));
        }
    }
    public void LegClickPunc()
    {
        clickCount++;
        clickCount = clickCount % 7;
        Broadcast("LegClickCommand", clickCount);


    }

    [OnCommand]
    public void LegClickCommand(int count)
    {
        buttonSound.Play();
        if(isFirst)
        {

            isFirst = false;
            Effect_one.SetActive(true);
            Effect_two.SetActive(true);
            electricSound.Play();
            Effect_one.GetComponent<ParticleSystem>().Play();
            Effect_two.GetComponent<ParticleSystem>().Play();
        }
        else
        {

            if(electricSound.isPlaying)
            {
                electricSound.Stop();
            }
            if (Effect_one.GetComponent<ParticleSystem>().isPlaying)
            {
                Effect_one.GetComponent<ParticleSystem>().Stop();
            }
            if (Effect_two.GetComponent<ParticleSystem>().isPlaying)
            {
                Effect_two.GetComponent<ParticleSystem>().Stop();
            }
        }


        if (count == 0)
        {
            Leg_Assemble.SetActive(true);
            Leg_Up.SetActive(false);
            Leg_Down.SetActive(false);
        }
        else if(count == 1)
        {
            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", true);
            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
            Leg_Assemble.GetComponent<Animator>().SetBool("EndTouch", false);
        }
        else if(count == 2)
        {
            rotationFlag = true;
        }
        else if (count == 3)
        {
            rotationFlag = false;
        }
        else if(count == 4)
        {
            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", false);
            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", true);
            Leg_Assemble.GetComponent<Animator>().SetBool("EndTouch", false);
        }
        else if(count == 5)
        {
            Leg_Assemble.GetComponent<Animator>().SetBool("FirstTouch", false);
            Leg_Assemble.GetComponent<Animator>().SetBool("AfterTouch", false);
            Leg_Assemble.GetComponent<Animator>().SetBool("EndTouch", true);

            Leg_Assemble.SetActive(false);
            Leg_Up.SetActive(true);
            Leg_Down.SetActive(false);
        }
        else if(count == 6)
        {
            Leg_Assemble.SetActive(false);
            Leg_Up.SetActive(false);
            Leg_Down.SetActive(true);
        }
        





    }
}
