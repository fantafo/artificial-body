﻿using FTF;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pvr_UnitySDKAPI;

public class BodyController : AutoCommander
{

    public GameObject Leg;
    public GameObject Hand;


    public GameObject Up;
    public GameObject Down;
    public GameObject Left;
    public GameObject Right;

    private bool isRotation;

    Coroutine rotationco;

    public GameObject BackPanel;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
#if !FTF_OBSERVER
        // 버튼을 눌렀을때
        if ((VRInput.ClickButtonUp || Input.GetMouseButtonUp(0) || Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER)) && ControllerDirHelper.isUp)
        {
            Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(FTFReticlePosition.Value));

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) // 레이 검출을 하고
            {
                if (hit.transform.gameObject == Leg)
                {
                    Leg.GetComponent<LegClick>().LegClickPunc();
                }
                else if (hit.transform.gameObject == Hand)
                {
                    Hand.GetComponent<HandClick>().HandClickPunc();
                }

                if (Physics.Raycast(ray, out hit)) 
                {
                    if (hit.transform.gameObject == Up)
                    {
                        Broadcast("up");
                    }
                    if (hit.transform.gameObject == Left)
                    {
                        Broadcast("left");
                    }
                    if (hit.transform.gameObject == Right)
                    {
                        Broadcast("right");
                    }
                    if (hit.transform.gameObject == Down)
                    {
                        Broadcast("down");
                    }
                }


            }
        }
#endif
    }

    public void ShowLeg()
    {
        Broadcast("broadcastShowLeg");
    }
    [OnCommand]
    public void broadcastShowLeg()
    {
        if(!BackPanel.activeSelf)
        {
            BackPanel.SetActive(true);
        }

        if (Hand.activeSelf)
        {
            Hand.SetActive(false);
        }

        if (!Leg.activeSelf)
        {
            Leg.SetActive(true);
        }
    }

    public void ShoeHand()
    {
        Broadcast("broadcastShowHand");
    }
    [OnCommand]
    public void broadcastShowHand()
    {
        if (!BackPanel.activeSelf)
        {
            BackPanel.SetActive(true);
        }


        if (Leg.activeSelf)
        {
            Leg.SetActive(false);
        }

        if (!Hand.activeSelf)
        {
            Hand.SetActive(true);
        }
    }

    [OnCommand]
    public void up()
    {
        if (!isRotation)
        {
            if (rotationco != null)
            {
                StopCoroutine(rotationco);
            }
            rotationco = StartCoroutine("RotateCube", new Vector3(90, 0, 0));
        }
    }
    [OnCommand]
    public void down()
    {
        if (!isRotation)
        {
            if (rotationco != null)
            {
                StopCoroutine(rotationco);
            }
            rotationco = StartCoroutine("RotateCube", new Vector3(-90, 0, 0));
        }
    }
    [OnCommand]
    public void left()
    {
        if (!isRotation)
        {
            if (rotationco != null)
            {
                StopCoroutine(rotationco);
            }
            rotationco = StartCoroutine("RotateCube", new Vector3(0, 90, 0));
        }
    }
    [OnCommand]
    public void right()
    {
        if (!isRotation)
        {
            if (rotationco != null)
            {
                StopCoroutine(rotationco);
            }
            rotationco = StartCoroutine("RotateCube", new Vector3(0, -90, 0));
        }
    }


    private IEnumerator RotateCube(Vector3 tempVector)
    {
        isRotation = true;
        for (int i = 0; i < 90; i++)
        {
            if(Leg.activeSelf)
            {
                Leg.transform.Rotate(new Vector3(tempVector.x / 90, tempVector.y / 90, 0), Space.World);
            }
            else if(Hand.activeSelf)
            {
                Hand.transform.Rotate(new Vector3(tempVector.x / 90, tempVector.y / 90, 0), Space.World);
            }
            yield return new WaitForSeconds(0.01f);
        }
        isRotation = false;
    }



}
