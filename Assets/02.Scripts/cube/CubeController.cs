﻿using FTF;
using FTF.Packet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pvr_UnitySDKAPI;

public class CubeController : AutoCommander
{
    public GameObject[] CubeArray;
    public GameObject ScriptBoard;

    public GameObject AssembleCube;

    public AudioSource[] coinSound;
    public AudioSource combineSound;

    public GameObject[] Cube_Assembled;
    public GameObject[] Cube_Pieces;

    public GameObject Cube_Assem_Scale;

    private bool isSelecting;
    private int movingPieces;

    private GameObject hand;
    private GameObject dot;
    private float distance;

    private Coroutine SelectEffect;
    private Coroutine MovingDataSender;
    private Coroutine UpDownCoroutine;

    private Coroutine AssemUpDownCor;
    private Coroutine scaleUpDown;

    private const float PositionUpdateInterval = 0.1f;
    private Vector3 beforePosition;
    private Vector3 nextPosition;
    private float PositionUpdateTimer;
    private bool isFirst;

    private float[] UpDown;
    private bool[] UpDownflag;

    private Color checkColor;
    private int secondPieceCount;

    public GameObject board;

    private float rotationWait;


    private float RotationAngle_Assem;
    private const float RotationAngle_Assem_Max = 70.0f;
    private float RotationWaitCount_Assem;
    private float RotationAngle_Piece;
    private const float RotationAngle_Piece_Max = 135.0f;
    private float RotationWaitCount_Piece;
    private const float RotationSpeed = 2.0f;
    private const float WaitSecond = 1.3f;

    private Coroutine AssemCubeRotatoinCor;
    private Coroutine CubePieceRotatoinCor;
    public GameObject AssemCubeRot;

    void Start()
    {
        RotationAngle_Assem = 0.0f;
        RotationAngle_Piece = 0.0f;
        RotationWaitCount_Assem = 0.0f;
        RotationWaitCount_Piece = 0.0f;

        secondPieceCount = 0;
        movingPieces = -1;
        isSelecting = false;
        hand = GameObject.Find("PvrController0/start");
        dot = GameObject.Find("customDot");
        checkColor.a = 152.0f / 255.0f;
        checkColor.r = 255.0f / 255.0f;
        checkColor.g = 0.0f / 255.0f;
        checkColor.b = 0.0f / 255.0f;
        UpDown = new float[8];
        for (int i = 0; i < UpDown.Length; i++)
        {
            UpDown[i] = Random.Range(0.0f, Mathf.PI);
        }
        UpDownCoroutine = StartCoroutine("UpDownCor");

    }

    void Update()
    {
#if !FTF_OBSERVER
        // 버튼을 눌렀을때
        if ((VRInput.ClickButtonUp || Input.GetMouseButtonUp(0) || Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER)) && ControllerDirHelper.isUp)
        {
            Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(FTFReticlePosition.Value));

            if (!isSelecting) // 현재 선택되어 있는 조각이 없다면
            {
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)) // 레이 검출을 하고
                {
                    int pieceNum = checkPieceNum(hit);  //몇번 조각인지 확인
                    if (pieceNum != -1)
                    {
                        Broadcast("selectPiece", pieceNum);
                        isSelecting = true;
                        MovingDataSender = StartCoroutine("MoveBroadcast");
                    }
                    if (hit.transform.gameObject == CubeArray[0])
                    {
                        Broadcast("CoinPutIn");
                    }
                }
            }
            else // 현재 어떤 조각을 선택중이라면 
            {
                RaycastHit hit;
                Broadcast("assembleEffectStop");
                if (MovingDataSender != null)
                {
                    StopCoroutine(MovingDataSender);
                }
                if (Physics.Raycast(ray, out hit)) // 레이 검출을 하고
                {
                    combinationCheck(hit);
                }
                Broadcast("selectCancle");

                if (Cube_Pieces[movingPieces].activeSelf)
                {
                    Cube_Pieces[movingPieces].GetComponent<Collider>().enabled = true;
                }
                //Cube_Pieces[movingPieces].GetComponent<Rigidbody>().isKinematic = false;
            }
        }
#endif

        if (isSelecting)
        {
            PositionUpdateTimer += Time.deltaTime;
            float ratio = PositionUpdateTimer / PositionUpdateInterval;
            if (ratio > 1.0f)
                ratio = 1.0f;
            if (isFirst && ratio < 1.0f)
            {
                Cube_Pieces[movingPieces].transform.position = Vector3.Lerp(beforePosition, nextPosition, PositionUpdateTimer / PositionUpdateInterval);
            }
            //movePiece(hand.transform.position + (dot.transform.position - hand.transform.position).normalized * 6.0f);

        }
        else
        {

        }


        

    }

    private int checkPieceNum(RaycastHit hit)   // 몇번 조각이 선택되었는지 확인해준다.
    {
        for (int i = 0; i < Cube_Pieces.Length; i++)
        {
            if (Cube_Pieces[i] != null)
            {
                if (hit.collider.gameObject == Cube_Pieces[i])
                {
                    Debug.Log(Cube_Pieces[i] + " 눌렀다!");
                    return i;
                }
            }
        }
        return -1;
    }

    private void combinationCheck(RaycastHit hit)
    {
        Debug.Log(hit.transform.gameObject.name);
        ; if (hit.transform.gameObject == GameObject.Find("Cube_Assembled"))
        {
            if (movingPieces == 0)
            {
                Broadcast("combination", 1);
            }
            if (movingPieces == 1)
            {
                Broadcast("combination", 2);
            }
            if (movingPieces == 2)
            {
                Broadcast("combination", 3);
            }
            if (movingPieces == 5)
            {
                Broadcast("combination", 4);
            }
        }
        else if (hit.transform.gameObject == Cube_Pieces[1])
        {
            if (movingPieces == 3)
            {
                Broadcast("combination", 5);
            }
            if (movingPieces == 4)
            {
                Broadcast("combination", 6);
            }
            if (movingPieces == 6)
            {
                Broadcast("combination", 7);
            }
        }
    }
    [OnCommand]
    public void combination(int combinationCase)
    {
        switch (combinationCase)
        {
            case 1:
                Cube_Assembled[5].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[8].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[9].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[14].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[15].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[16].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Assembled[17].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[0].SetActive(false);
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Assem_Scale);
                combineSound.Play();
                break;
            case 2:
                if (secondPieceCount >= 3)
                {
                    Cube_Assembled[0].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[1].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[2].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[3].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[4].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[6].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[7].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Assembled[13].GetComponent<MaterialController>().MaterialChange(1);
                    Cube_Pieces[1].SetActive(false);
                    scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Assem_Scale);
                    combineSound.Play();
                }
                break;
            case 3:
                Cube_Assembled[10].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[2].SetActive(false);
                RotationWaitCount_Assem = WaitSecond;
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Assem_Scale);
                combineSound.Play();
                break;
            case 4:
                Cube_Assembled[11].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[5].SetActive(false);
                RotationWaitCount_Assem = WaitSecond;
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Assem_Scale);
                combineSound.Play();
                break;
            case 5:
                Cube_Pieces[1].GetComponent<SubCombine>().subPiece[1].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[3].SetActive(false);
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Pieces[1]);
                secondPieceCount++;
                combineSound.Play();
                break;
            case 6:
                Cube_Pieces[1].GetComponent<SubCombine>().subPiece[2].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[4].SetActive(false);
                RotationWaitCount_Piece = WaitSecond;
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Pieces[1]);
                secondPieceCount++;
                combineSound.Play();
                break;
            case 7:
                Cube_Pieces[1].GetComponent<SubCombine>().subPiece[3].GetComponent<MaterialController>().MaterialChange(1);
                Cube_Pieces[6].SetActive(false);
                scaleUpDown = StartCoroutine("ScaleUpDown", Cube_Pieces[1]);
                secondPieceCount++;
                combineSound.Play();
                break;
            default:
                break;
        }
    }

    [OnCommand]
    public void selectPiece(int PiecesNum)  // 몇번 조각이 선택되었는지 얻어온다.
    {
        isSelecting = true;
        movingPieces = PiecesNum;
        Cube_Pieces[movingPieces].GetComponent<Collider>().enabled = false;

        if(movingPieces == 5)
        {
            if(AssemCubeRotatoinCor != null)
            {
                StopCoroutine(AssemCubeRotatoinCor);
            }
            AssemCubeRotatoinCor = StartCoroutine("AssemCubeRotator");
        }
        if (movingPieces == 2)
        {
            if (AssemCubeRotatoinCor != null)
            {
                StopCoroutine(AssemCubeRotatoinCor);
            }
            AssemCubeRotatoinCor = StartCoroutine("AssemCubeRotator");
        }
        else if(movingPieces ==4)
        {
            if (CubePieceRotatoinCor != null)
            {
                StopCoroutine(CubePieceRotatoinCor);
            }
            CubePieceRotatoinCor = StartCoroutine("CubePieceRotator");
        }


        if (SelectEffect != null)
            StopCoroutine(SelectEffect);
        SelectEffect = StartCoroutine("assembleEffect", PiecesNum);

        isFirst = false;
        //Cube_Pieces[movingPieces].GetComponent<Rigidbody>().isKinematic = true ;
    }

    [OnCommand]
    public void movePiece(float x, float y, float z)
    {
        Vector3 ObjectPosition = new Vector3(x, y, z);
       
        PositionUpdateTimer = 0.0f;
        beforePosition = Cube_Pieces[movingPieces].transform.position;
        nextPosition = ObjectPosition;
        isFirst = true;
    }

    [OnCommand]
    public void selectCancle()
    {
        isSelecting = false;
    }

    private IEnumerator assembleEffect(int pieceNum)
    {
        List<GameObject> SelectedPieces = new List<GameObject>();
        switch (pieceNum)
        {
            case 0:
                SelectedPieces.Add(Cube_Assembled[5]);
                SelectedPieces.Add(Cube_Assembled[8]);
                SelectedPieces.Add(Cube_Assembled[14]);
                SelectedPieces.Add(Cube_Assembled[15]);
                SelectedPieces.Add(Cube_Assembled[16]);
                SelectedPieces.Add(Cube_Assembled[17]);
                break;
            case 1:
                SelectedPieces.Add(Cube_Assembled[0]);
                SelectedPieces.Add(Cube_Assembled[1]);
                SelectedPieces.Add(Cube_Assembled[2]);
                SelectedPieces.Add(Cube_Assembled[3]);
                SelectedPieces.Add(Cube_Assembled[4]);
                SelectedPieces.Add(Cube_Assembled[6]);
                SelectedPieces.Add(Cube_Assembled[7]);
                SelectedPieces.Add(Cube_Assembled[13]);
                break;
            case 2:
                SelectedPieces.Add(Cube_Assembled[10]);
                break;
            case 3:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[1]);
                break;
            case 4:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[2]);
                break;
            case 5:
                SelectedPieces.Add(Cube_Assembled[11]);
                break;
            case 6:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[3]);
                break;
            default:
                break;

        }

        float color = 0.6f;
        bool colorPlusMinus = true;
        while (isSelecting)
        {
            if (colorPlusMinus)
            {
                color -= 0.01f;
            }
            else
            {
                color += 0.01f;
            }
            if (color - 0.01f < 0.6f)
            {
                colorPlusMinus = false;
            }
            else if (color + 0.01f > 1.0f)
            {
                colorPlusMinus = true;
            }

            for (int i = 0; i < SelectedPieces.Count; i++)
            {
                Color tempColor = SelectedPieces[i].GetComponent<MaterialController>().mymaterial[0].GetColor("_Color");
                tempColor.r = Mathf.Lerp(tempColor.r, checkColor.r, color);
                tempColor.g = Mathf.Lerp(tempColor.g, checkColor.g, color);
                tempColor.b = Mathf.Lerp(tempColor.b, checkColor.b, color);
                tempColor.a = Mathf.Lerp(tempColor.a, checkColor.a, color);

                SelectedPieces[i].GetComponent<MeshRenderer>().material.SetColor("_Color", tempColor);
            }
            yield return new WaitForSeconds(0.01f);

        }

        yield return null;
    }

    [OnCommand]
    private void assembleEffectStop()
    {
        List<GameObject> SelectedPieces = new List<GameObject>();
        switch (movingPieces)
        {
            case 0:
                SelectedPieces.Add(Cube_Assembled[5]);
                SelectedPieces.Add(Cube_Assembled[8]);
                SelectedPieces.Add(Cube_Assembled[14]);
                SelectedPieces.Add(Cube_Assembled[15]);
                SelectedPieces.Add(Cube_Assembled[16]);
                SelectedPieces.Add(Cube_Assembled[17]);
                break;
            case 1:
                SelectedPieces.Add(Cube_Assembled[0]);
                SelectedPieces.Add(Cube_Assembled[1]);
                SelectedPieces.Add(Cube_Assembled[2]);
                SelectedPieces.Add(Cube_Assembled[3]);
                SelectedPieces.Add(Cube_Assembled[4]);
                SelectedPieces.Add(Cube_Assembled[6]);
                SelectedPieces.Add(Cube_Assembled[7]);
                SelectedPieces.Add(Cube_Assembled[13]);
                break;
            case 2:
                SelectedPieces.Add(Cube_Assembled[10]);
                break;
            case 3:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[1]);
                break;
            case 4:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[2]);
                break;
            case 5:
                SelectedPieces.Add(Cube_Assembled[11]);
                break;
            case 6:
                SelectedPieces.Add(Cube_Pieces[1].GetComponent<SubCombine>().subPiece[3]);
                break;
            default:
                break;
        }
        for (int i = 0; i < SelectedPieces.Count; i++)
        {
            Color tempColor = SelectedPieces[i].GetComponent<MaterialController>().mymaterial[0].GetColor("_Color");

            SelectedPieces[i].GetComponent<MeshRenderer>().material.SetColor("_Color", tempColor);
        }
        StopCoroutine(SelectEffect);
    }
    private IEnumerator MoveBroadcast()
    {
        while (isSelecting)
        {
            Vector3 tmpPosition = hand.transform.position + (dot.transform.position - hand.transform.position).normalized * 11.0f;

            Broadcast("movePiece", tmpPosition.x, tmpPosition.y, tmpPosition.z);
            //Debug.Log(tmpPosition);
            yield return new WaitForSeconds(PositionUpdateInterval);
        }
        yield return null;
    }

    private IEnumerator UpDownCor()
    {
        while (true)
        {
            if (CubeArray[3].activeSelf)
            {
                for (int i = 0; i < Cube_Pieces.Length; i++)
                {
                    if (isSelecting)
                    {
                        if (i != movingPieces)
                        {
                            Cube_Pieces[i].transform.position += new Vector3(0.0f, Mathf.Sin(UpDown[i]) * 0.02f, 0.0f);
                            UpDown[i] += 0.1f;

                            if (UpDown[i] > Mathf.PI * 2.0f)
                            {
                                UpDown[i] = 0.0f;
                            }
                        }
                    }
                    else
                    {
                        Cube_Pieces[i].transform.position += new Vector3(0.0f, Mathf.Sin(UpDown[i]) * 0.02f, 0.0f);
                        UpDown[i] += 0.1f;

                        if (UpDown[i] > Mathf.PI * 2.0f)
                        {
                            UpDown[i] = 0.0f;
                        }
                    }


                }

            }
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }

    private IEnumerator AssembleUpDown()
    {
        float updown = 0.0f;
        while (true)
        {
            AssembleCube.transform.position += new Vector3(0.0f, Mathf.Sin(updown) * 0.02f, 0.0f);
            updown += 0.03f;
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }

    private IEnumerator CoinPutInCor()
    {
        yield return new WaitForSeconds(0.3f);
        coinSound[1].Play();
        yield return new WaitForSeconds(0.6f);
        coinSound[0].Play();
        yield return new WaitForSeconds(3.0f);
        board.GetComponent<BoardController>().isCoinEnd = true;

        yield return null;
    }

    public void BroadcastShowCube1()
    {
        Broadcast("ShowCube1");
    }
    public void BroadcastShowCube2()
    {
        Broadcast("ShowCube2");
    }
    public void BroadcastShowCube3()
    {
        Broadcast("ShowCube3");
    }


    [OnCommand]
    public void ShowCube1()
    {
        for (int i = 0; i < CubeArray.Length; i++)
        {
            CubeArray[i].SetActive(false);
        }
        CubeArray[0].SetActive(true);
    }

    [OnCommand]
    public void ShowCube2()
    {
        for (int i = 0; i < CubeArray.Length; i++)
        {
            CubeArray[i].SetActive(false);
        }
        CubeArray[1].SetActive(true);
    }

    [OnCommand]
    public void ShowCube3()
    {
        if (ScriptBoard.activeSelf)
        {
            ScriptBoard.SetActive(false);
        }
        for (int i = 0; i < CubeArray.Length; i++)
        {
            CubeArray[i].SetActive(false);
        }
        CubeArray[2].SetActive(true);
        CubeArray[3].SetActive(true);
    }

    [OnCommand]
    public void CoinPutIn()
    {
        CubeArray[0].GetComponentInChildren<Animator>().SetBool("start", true);
        StartCoroutine("CoinPutInCor");
    }


    private IEnumerator ScaleUpDown(GameObject cubePiece)
    {
        float pieceScale = 1.0f;

        while(pieceScale < 1.5f)
        {
            pieceScale += 0.05f;
            cubePiece.transform.localScale = new Vector3(pieceScale, pieceScale, pieceScale);

            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(0.3f);

        while (pieceScale >= 1.0f)
        {
            pieceScale -= 0.05f;
            cubePiece.transform.localScale = new Vector3(pieceScale, pieceScale, pieceScale);

            yield return new WaitForSeconds(0.01f);
        }

        cubePiece.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        yield return null;
    }
    
    private IEnumerator AssemCubeRotator()
    {

        while (RotationAngle_Assem < RotationAngle_Assem_Max)
        {
            RotationAngle_Assem += RotationSpeed;
            Cube_Assem_Scale.transform.Rotate(RotationSpeed, 0, 0,Space.Self);
            yield return new WaitForSeconds(0.01f);
            if (isSelecting == false)
                break;
        }
        while(isSelecting ==true)
        {
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(RotationWaitCount_Assem);

        while (RotationAngle_Assem > 0)
        {
            RotationAngle_Assem -= RotationSpeed;
            Cube_Assem_Scale.transform.Rotate(-RotationSpeed, 0, 0, Space.Self);
            yield return new WaitForSeconds(0.01f);

        }
        yield return null;
    }
    private IEnumerator CubePieceRotator()
    {
        while (RotationAngle_Piece < RotationAngle_Piece_Max)
        {
            RotationAngle_Piece += RotationSpeed;
            Cube_Pieces[1].transform.Rotate(-RotationSpeed, 0, 0, Space.Self);
            yield return new WaitForSeconds(0.01f);
            if (isSelecting == false)
                break;
        }
        while(isSelecting == true)
        {
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(RotationWaitCount_Piece);

        while (RotationAngle_Piece > 0)
        {
            RotationAngle_Piece -= RotationSpeed;
            Cube_Pieces[1].transform.Rotate(RotationSpeed, 0, 0, Space.Self);
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }

}
