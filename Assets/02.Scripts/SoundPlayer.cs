﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
public class SoundPlayer : AutoCommander
{

    public AudioSource[] AudioSourceArray;

    public void BroadcastAudioPlay_1()
    {
        Broadcast("AudioPlay_1");
    }

    [OnCommand]
    public void AudioPlay_1()
    {
        AudioSourceArray[0].Play();
    }

    public void BroadcastAudioPlay_2()
    {
        Broadcast("AudioPlay_2");
    }

    [OnCommand]
    public void AudioPlay_2()
    {
        AudioSourceArray[1].Play();
    }

    
}
