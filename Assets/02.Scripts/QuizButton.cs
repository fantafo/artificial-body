﻿using FTF;
using FTF.Packet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizButton : AutoCommander
{
    public GameObject quiz1;
    public GameObject Cube;

	public void makeQuiz_1()
    {
        Broadcast("showQuiz_1");
    }
    
    
    [OnCommand]
    public void showQuiz_1()
    {
        quiz1.GetComponent<Exam>().CubeShow();
        for (int i = 0; i< Cube.GetComponent<CubeController>().CubeArray.Length; i++)
        {
            if (Cube.GetComponent<CubeController>().CubeArray[i] != null && Cube.GetComponent<CubeController>().CubeArray[i].activeSelf)
            {
                Cube.GetComponent<CubeController>().CubeArray[i].SetActive(false);
            }
        }
    }
}
