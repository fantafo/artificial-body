﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPointerEnterExit : MonoBehaviour {


    public void OnPointerEnter()
    {
        iTween.ScaleTo(gameObject, Vector3.one * 1.5f, 1.0f);
        if(transform.parent.gameObject.GetComponent<BoxCollider>() != null)
            transform.parent.gameObject.GetComponent<BoxCollider>().size = new Vector3(transform.parent.gameObject.GetComponent<BoxCollider>().size.x * 1.5f, transform.parent.gameObject.GetComponent<BoxCollider>().size.y * 1.5f, transform.parent.gameObject.GetComponent<BoxCollider>().size.z);
    }

    public void OnPointerExit()
    {
        if (transform.parent.gameObject.GetComponent<BoxCollider>() != null)
            transform.parent.gameObject.GetComponent<BoxCollider>().size = new Vector3(transform.parent.gameObject.GetComponent<BoxCollider>().size.x / 1.5f, transform.parent.gameObject.GetComponent<BoxCollider>().size.y / 1.5f, transform.parent.gameObject.GetComponent<BoxCollider>().size.z);

        iTween.ScaleTo(gameObject, Vector3.one, 1.0f); 
    }

    public void OnPointerClick()
    {
        gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
}
